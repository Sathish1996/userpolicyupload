
export class InsuranceName{
    public insurances:String[];

    public carBrand:String[];

    public relationList:String[];

    public getRelationList(): String[] {
        return this.relationList;
    }

    public setRelationList(relationList: String[]): void {
        this.relationList = relationList;
    }


    public getCarBrand(): String[] {
        return this.carBrand;
    }

    public setCarBrand(carBrand: String[]): void {
        this.carBrand = carBrand;
    }



    setInsurance(insurances: String[]) {
        this.insurances=insurances;
    }


    getInsurance():String[]{
        
        return this.insurances;
    }
}