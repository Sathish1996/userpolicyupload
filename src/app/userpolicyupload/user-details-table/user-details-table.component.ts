import { Title }     from '@angular/platform-browser';
import { Router, NavigationEnd, NavigationStart, ActivatedRoute } from '@angular/router';
import * as global from '../globals';
import { Component, OnInit, HostListener, ViewChild, Renderer2 } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import { RequestUserDocumentService } from '../request-user-document.service';
import { ResponseUserDocument } from '../model/ResponseUserDocument';
import { UserPolicy } from '../model/UserPolicy';
import { User } from '../model/user';
import { ExternalEntityType } from '../model/External_entity_type';
import { External_entity_name } from '../model/External_entity_name';
import { Relation } from '../model/Relation';
import { User_policy_document } from '../model/user_policy_document';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { MatTableDataSource } from '@angular/material/table';
import { SubmitService } from '../submit.service';
import { InsuranceName } from '../insuranceName';
import {  NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
declare var jQuery: any;

// export class MyErrorStateMatcher implements ErrorStateMatcher {
//   isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
//     const isSubmitted = form && form.submitted;
//     return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
//   }
// }

@Component({
  selector: 'app-user-details-table',
  templateUrl: './user-details-table.component.html',
  styleUrls: ['./user-details-table.component.css']
})
export class UserDetailsTableComponent implements OnInit {
  pageSettings;
  closeResult: string;
  tableflag = true;
  flagforTravel = false;
  flagforTermLife  = false;
  flagforHome = false;
  flagforCar = false;
  flagforTwoWheeler = false;
  flagforWholeLife = false;
  flagforULIP = false;
  flagforFire = false;
  flagforPersonalAccident = false;
  selectedIndex = 0;
  flagforHealth: boolean = false;
  flagforEndowm: boolean = false;
  flagforRetirement: boolean = false;
  deletedata: any;
  instance: any;
  instance1: any;
  instance2: any;
  instance3: any;
  instance4: any;
  instance5: any;
  instance6: any;
  instance7: any;
  instance8: any;
  instance9: any;
  instance10: any;
  instance11: any;
  instance12: any;
  response: string[]=[];
  isDataAvailable:boolean = false;
  displayedColumns = ['position', 'mobileNo', 'entityType', 'entityName','documentPath','deactivate'];
  dataSource = new MatTableDataSource(this.response);
  message:String='';
 loading=false;
 i: number;
 relationList:String[]=[];
 isErrorMessage= false;
 errorMessage:String="";
 policyDocument:User_policy_document[]=[];
  urlformodel: any;
  Success: any;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator; 
  url: any;

  @HostListener('window:popstate', ['$event'])
  onPopState(event:any) {
  
  //this.router.navigate(['userDetails']);
    //this.openDialog1();
  }




  fetchEvent(): Promise<any> {
    return this.service.getUserDocument().then((data:any)=>
    {
      if(data.errorMessage)
      {
        this.isErrorMessage=true;
      this.errorMessage=data.errorMessage;
      console.log(this.errorMessage);
      }
      else{
        this.isErrorMessage=false;
        console.log(data);
      this.responseUserDocument=data;
      this.response=data.userDocumentList;
      console.log(this.response)
     this.dataSource = new MatTableDataSource(this.response);
     console.log(this.dataSource)
      this.loading = false
    }});
}

showadd(data){
  this.tableflag = false;
  
  this.instance = jQuery('.agentForm').parsley();
  console.log(this.instance)
  this.urlformodel = data.documentPath;
  sessionStorage.setItem('urlformodel', data.documentPath);
  this.external_entity_name.setExternal_entity_name_id(data.external_entity_name.externalEntityNameId);
  this.external_entity_type.setExternalEntityTypeId(data.external_entity_type.externalEntityTypeId);
  this.user.setMobileNo(data.user.mobileNo);
  this.userPolicy.setExternal_entity_name(this.external_entity_name);
  this.userPolicy.setExternal_entity_type(this.external_entity_type);
  this.userPolicy.setUser(this.user);
  this.userPolicy.setGenericIdentifier(data.userDocumentId);
  this.name.setRelationList(this.relationList);
  this.userPolicyDocument.documentPath=(data.documentPath);
  this.policyDocument.push(this.userPolicyDocument);
  this.userPolicy.setUser_policy_document(this.policyDocument);
  this.router.navigate(['userpolicyupload/userform']);
  //this.userPolicy.setUser_policy_document(this.userPolicyDocument);
  //this.userForm.getUserDocument(this.userPolicy,this.insuranceName);
}

//modalReference: any;

open(content,url) {
  console.log(url)
  this.urlformodel = url;
  this.modalService.open(content,{ size: 'lg' }).result.then((result) => {
    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
}

modalReference: NgbModalRef;

open1(content,data) {
  console.log(data)
  this.deletedata = data
  this.modalReference = this.modalService.open(content);
  this.modalReference.result.then((result) => {
    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
  }
  
  
  JoinAndClose() {
    console.log(this.deletedata.userDocumentId)
    this.service.deactivateDocument(this.deletedata.userDocumentId).then((data:any)=>
    {
      console.log(data)
      this.modalReference.close();
      this.Success=data.successMessage;
      Swal.fire({  
        icon: 'success',  
        title: 'Success',  
        text: this.Success,    
      })
      this.fetchEvent().then(()=>
    {this.isDataAvailable = true;
      
      this.dataSource.paginator = this.paginator;
      console.log(this.dataSource.paginator)
      //this.loading=false;
      
      this.submitService.userRelation().then((data:any)=>
      {
       console.log(data)
        this.relation.setRelationship_to_insured(data.relationship_to_insured);
        
        this.i=0;
        for(let event of this.relation.getRelationship_to_insured())
        {
           this.relationList[this.i]= event.relation;
  
           this.i++;
        } 
        this.loading=false;
      });
    }
      );
    })
  

  }

  JoinAndClose1() {
    this.modalReference.close();
    }

private getDismissReason(reason: any): string {
  if (reason === ModalDismissReasons.ESC) {
    return 'by pressing ESC';
  } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
    return 'by clicking on a backdrop';
  } else {
    return  `with: ${reason}`;
  }
}
  constructor(
    private modalService: NgbModal,
    private service:RequestUserDocumentService,private responseUserDocument:ResponseUserDocument,
    private userPolicy:UserPolicy,private user:User,private external_entity_type:ExternalEntityType,
    private external_entity_name:External_entity_name,
    private router: Router,
    private relation:Relation,
    private userPolicyDocument:User_policy_document,
    private submitService:SubmitService,
    private name: InsuranceName, private renderer: Renderer2) {
      
     }

     

  ngOnInit() {
    this.loading=true;
    //window.history.pushState( {} , 'userDetails','#/userDetails');
    this.fetchEvent().then(()=>
    {this.isDataAvailable = true;
      
      this.dataSource.paginator = this.paginator;
      console.log(this.dataSource.paginator)
      //this.loading=false;
      
      this.submitService.userRelation().then((data:any)=>
      {
       console.log(data)
        this.relation.setRelationship_to_insured(data.relationship_to_insured);
        
        this.i=0;
        for(let event of this.relation.getRelationship_to_insured())
        {
           this.relationList[this.i]= event.relation;
  
           this.i++;
        } 
        //this.loading=false;
      });
    }
      );
      //this.loading=true;
        //getting the insurance name from the user-form component according to the insurance type selected
        
        this.pageSettings = {
          pageSidebarMinified: false,
          pageContentFullHeight: false,
          pageContentFullWidth: false,
          pageContentInverseMode: false,
          pageWithFooter: false,
          pageWithoutSidebar: false,
          pageSidebarRight: false,
          pageSidebarRightCollapsed: false,
          pageSidebarTwo: false,
          pageSidebarWide: false,
          pageSidebarTransparent: false,
          pageSidebarLight: false,
          pageTopMenu: false,
          pageEmpty: false,
          pageBodyWhite: false,
          pageMobileSidebarToggled: false,
          pageMobileSidebarFirstClicked: false,
          pageMobileSidebarRightToggled: false,
          pageMobileSidebarRightFirstClicked: false
        };


  }


  
	// window scroll
  pageHasScroll;
  @HostListener('window:scroll', ['$event'])
  onWindowScroll($event) {
    var doc = document.documentElement;
    var top = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);
    if (top > 0) {
      this.pageHasScroll = true;
    } else {
      this.pageHasScroll = false;
    }
  }

	// clear settings to default
	clearSettings() {
		this.pageSettings.pageSidebarMinified = false;
		this.pageSettings.pageContentFullHeight = false,
		this.pageSettings.pageContentFullWidth = false;
		this.pageSettings.pageWithFooter = false;
		this.pageSettings.pageWithoutSidebar = false;
		this.pageSettings.pageSidebarRight = false;
		this.pageSettings.pageSidebarRightCollapsed = false;
		this.pageSettings.pageSidebarTwo = false;
		this.pageSettings.pageSidebarWide = false;
		this.pageSettings.pageSidebarTransparent = false;
		this.pageSettings.pageSidebarLight = false;
		this.pageSettings.pageTopMenu = false;
		this.pageSettings.pageEmpty = false;
		this.pageSettings.pageBodyWhite = false;
		this.pageSettings.pageContentInverseMode = false;
		this.pageSettings.pageMobileSidebarToggled = false;
		this.pageSettings.pageMobileSidebarFirstClicked = false;
		this.pageSettings.pageMobileRightSidebarToggled = false;
		this.pageSettings.pageMobileRightSidebarFirstClicked = false;
  	this.renderer.removeClass(document.body, 'bg-white');
	}

	// set page settings
  setPageSettings(settings) {
  	for (let option in settings) {
  		this.pageSettings[option] = settings[option];
  		if (option == 'pageBodyWhite' && settings[option] == true) {
  		  this.renderer.addClass(document.body, 'bg-white');
  		}
		}
  }

  // set page minified
  onToggleSidebarMinified(val: boolean):void {
  	if (this.pageSettings.pageSidebarMinified) {
  		this.pageSettings.pageSidebarMinified = false;
  	} else {
  		this.pageSettings.pageSidebarMinified = true;
  	}
	}

  // set page right collapse
  onToggleSidebarRight(val: boolean):void {
  	if (this.pageSettings.pageSidebarRightCollapsed) {
  		this.pageSettings.pageSidebarRightCollapsed = false;
  	} else {
  		this.pageSettings.pageSidebarRightCollapsed = true;
  	}
	}

  // hide mobile sidebar
  onHideMobileSidebar(val: boolean):void {
    if (this.pageSettings.pageMobileSidebarToggled) {
      if (this.pageSettings.pageMobileSidebarFirstClicked) {
        this.pageSettings.pageMobileSidebarFirstClicked = false;
      } else {
  		  this.pageSettings.pageMobileSidebarToggled = false;
      }
    }
	}

  // toggle mobile sidebar
  onToggleMobileSidebar(val: boolean):void {
    if (this.pageSettings.pageMobileSidebarToggled) {
  		this.pageSettings.pageMobileSidebarToggled = false;
    } else {
  		this.pageSettings.pageMobileSidebarToggled = true;
  		this.pageSettings.pageMobileSidebarFirstClicked = true;
    }
	}


  // hide right mobile sidebar
  onHideMobileRightSidebar(val: boolean):void {
    if (this.pageSettings.pageMobileRightSidebarToggled) {
      if (this.pageSettings.pageMobileRightSidebarFirstClicked) {
        this.pageSettings.pageMobileRightSidebarFirstClicked = false;
      } else {
  		  this.pageSettings.pageMobileRightSidebarToggled = false;
      }
    }
	}

  // toggle right mobile sidebar
  onToggleMobileRightSidebar(val: boolean):void {
    if (this.pageSettings.pageMobileRightSidebarToggled) {
  		this.pageSettings.pageMobileRightSidebarToggled = false;
    } else {
  		this.pageSettings.pageMobileRightSidebarToggled = true;
  		this.pageSettings.pageMobileRightSidebarFirstClicked = true;
    }
	}

  
}
