import { Injectable } from '@angular/core';
import { UserPolicy } from './model/UserPolicy';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoginMobilenoComponent } from './login-mobileno/login-mobileno.component';
import { RequestRelation } from './model/RequestRelation';
import { BaseServiceService } from './base-service.service';

@Injectable({
  providedIn: 'root'
})
export class SubmitService {



  private options = { headers: new HttpHeaders().set('Content-Type', 'application/json')  
  .set('authentication-token',sessionStorage.getItem("password"))
  .set('authentication-username',sessionStorage.getItem("userId"))
   };
   
 submitData(data:UserPolicy ){
  
  data.setServiceName("UserPolicyService");
  data.setServiceMethod("saveOrUpdateUserPolicy");
  data.setClientKey("f6b720f6707cd1c29b5bd3b5123f8115aeef13d8f34f9a757c0dca91c3e26af3");
  
  data.setLoggedInUserId(this.loginMobilenoComponent.sendResponseLogin().getUserId());
  data.setClientId(sessionStorage.getItem("clientId"));
  data.setVersion(this.loginMobilenoComponent.sendResponseLogin().getVersion())

 console.log(data);
  return this.http.post(this.baseService.baseUrl(),JSON.stringify(data),this.options);
 }

 userRelation(){
   this.requestRelation.setClientId(sessionStorage.getItem("clientId"));
   
  this.requestRelation.setClientKey("f6b720f6707cd1c29b5bd3b5123f8115aeef13d8f34f9a757c0dca91c3e26af3");
   this.requestRelation.setVersion(this.loginMobilenoComponent.sendResponseLogin().getVersion());
   this.requestRelation.setLoggedInUserId(this.loginMobilenoComponent.sendResponseLogin().getUserId());
   this.requestRelation.setServiceName("FinsAllService");
   this.requestRelation.setServiceMethod("getRelationshipToInsured");
   this.requestRelation.setClientKey("f6b720f6707cd1c29b5bd3b5123f8115aeef13d8f34f9a757c0dca91c3e26af3");
  
   return this.http.post(this.baseService.baseUrl(),JSON.stringify(this.requestRelation),this.options).toPromise();
 }
  constructor(private http: HttpClient,private request: UserPolicy, private requestRelation:RequestRelation,
    private loginMobilenoComponent:LoginMobilenoComponent,private baseService:BaseServiceService) { }
}
