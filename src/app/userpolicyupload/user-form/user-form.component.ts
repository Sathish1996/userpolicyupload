
//import { sha256} from 'js-sha256';
import { BaseServiceService } from '../base-service.service';
import { Title }     from '@angular/platform-browser';
import { Router, NavigationEnd, NavigationStart, ActivatedRoute } from '@angular/router';
import * as global from '../globals';
import { Component, OnInit, HostListener, ViewChild, Renderer2 } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import { RequestUserDocumentService } from '../request-user-document.service';
import { ResponseUserDocument } from '../model/ResponseUserDocument';
import { UserPolicy } from '../model/UserPolicy';
import { User } from '../model/user';
import { ExternalEntityType } from '../model/External_entity_type';
import { External_entity_name } from '../model/External_entity_name';
import { ResponseLogin } from '../model/ResponseLogin';
import { LoginMobilenoComponent } from '../login-mobileno/login-mobileno.component';
import { delay } from 'rxjs/operators';
import { Relation } from '../model/Relation';
import { User_policy_document } from '../model/user_policy_document';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { MatTableDataSource } from '@angular/material/table';
import { SubmitService } from '../submit.service';
import { NgForm } from '@angular/forms';
import { ResponseExternalEntityName } from '../model/ResponseExternalEntityName';
import { InsuranceName } from '../insuranceName';
import { User_relation } from '../model/User_relation';
import { VehicleDetailsService } from '../vehicle-details.service';
import { ResponseVehicleBrand } from '../model/ResponseVehicleBrand';
import {ErrorStateMatcher} from '@angular/material/core';
import { external_entity_name_list } from '../model/External_Entity_Name_List';
import { UserPolicyCoverage } from '../model/User_policy_coverage';
import { UserPolicyAgent } from '../model/User_policy_agent';
import { User_policy_vehicle_details } from '../model/User_policy_vehicle_details';
import { ResponseVehicleModel } from '../model/ResponseVehicleModel';
import { DatePipe } from '@angular/common';
import {  NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
declare var jQuery: any;
import { DataService } from '../dataServicefortoggle'

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
//this component is for login using User registered mobile number and password
export class UserFormComponent implements OnInit {
    pageSettings;
    closeResult: string;
    tableflag = false;
    flagforTravel = false;
    flagforTermLife  = false;
    flagforHome = false;
    flagforCar = false;
    flagforTwoWheeler = false;
    flagforWholeLife = false;
    flagforULIP = false;
    flagforFire = false;
    flagforPersonalAccident = false;
    selectedIndex = 0;
    flagforHealth: boolean = false;
    flagforEndowm: boolean = false;
    flagforRetirement: boolean = false;
    deletedata: any;
    instance: any;
    instance1: any;
    instance2: any;
    instance3: any;
    instance4: any;
    instance5: any;
    instance6: any;
    instance7: any;
    instance8: any;
    instance9: any;
    instance10: any;
    instance11: any;
    instance12: any;
  
  
    selectTab(index: number): void {
      console.log(index)
      this.selectedIndex = index;
      if(index == 1){
        this.isRequired = false;
        this.getInsuranceName(this.name,this.userPolicy,this.responseEntityName,this.responseVehicleBrand)
          {
              
            this.insurances=this.name.getInsurance();
            this.responseData=this.responseEntityName;
              this.responseVehicle=this.responseVehicleBrand;
             
              this.i=0;
              for(let event of this.userPolicy.getUser_policy_document()) 
              {
                this.urlArray[this.i]=event.documentPath;
                this.i++;
              }
              this.url=this.urlArray[0];
               this.urlType = this.urlArray[0].split(".").pop();
      
              //checking if the insurance type selected is car, two wheeler or others
              if(this.userPolicy.getExternal_entity_type().getExternalEntityTypeId()==1 ||
               this.userPolicy.getExternal_entity_type().getExternalEntityTypeId()==2 )
               {
                 this.isMotor=true;
                 this.i=0;
                
                 for(let event of this.responseVehicleBrand.getVehicle_brand())
                 {
                     this.carMakers[this.i]=event.brand;
                     this.i++;
                 }
                 
               }
          }
      }
    }
  
    response: string[]=[];
    isDataAvailable:boolean = false;
    displayedColumns = ['position', 'mobileNo', 'entityType', 'entityName','documentPath','deactivate'];
     dataSource = new MatTableDataSource(this.response);
    message:String='';
   loading=false;
   i: number;
   relationList:String[]=[];
   isErrorMessage= false;
   errorMessage:String="";
   policyDocument:User_policy_document[]=[];
   urlformodel:any;
  
   @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator; 
    url: any;
  
    ingredients: String[]=[];
    insurances: String[]=[];
    carBrand:String[]=[];
     rootId: String[]=[];
     externalEntityTypeId: any;
     externalEntityNameId: String;
     insuranceroot: any=[];
     selectedValue: string='';
     entityType: String;
     id: string;
     isRequired: boolean=false;
    // registerForm: FormGroup;
     activeImage:number=0;
     urlArray:String[]=[];
     minDate = new Date(1900,0,1);
     maxDate = new Date(2020,0,1);
    // matcher = new MyErrorStateMatcher();
     urlType: string;
     active = 1;   
  
   
     fillRequired() //checking if all required fields are filled or not
     {
    this.isRequired=true;
     }
   
    getUserDocument(userPolicy:UserPolicy,insuranceName:InsuranceName)
    {
   
    }
     getResponse(){} //implemented inside onInit() to get the names of the type of Insurance through API
   
   
     onTypeSelected(selectedValue:string) //on selecting insurance type
     {
       this.selectedValue=selectedValue;//storing the insurance type selected inside selectedValue variable
       this.isRequired=false;
       
     }
   nextImage()
   {
     const next= this.activeImage +1 >= this.urlArray.length-1 ?this.urlArray.length -1 :this.activeImage+1;
   
     this.url=this.urlArray[next];
   
   }
   
   previousImage()
   {
     const prev= this.activeImage -1 <0 ? 0 : this.activeImage -1;
   
     this.url=this.urlArray[prev];
   }
   
     userPolicyId(event:any)
     {
       this.userPolicy.setUserPolicyId(event.target.value);
     }
   
     logOut()
     {
       this.responseLogin.setUserId(undefined);
       this.responseLogin.setErrorMessage("Please login again to continue");
       this.loginComponent.getResponseLogin(this.responseLogin);
       
       this.router.navigate([""]);
     }
   
     urlExists(url) {
       return fetch(url, {mode: "no-cors"})
         .then(res => true)
         .catch(err => false)
     }
  
     fName(event: any){
      this.userRelation.setFirstName(event.target.value);
      this.isRequired=false;
    }
  
   
    //getting Lat name from user
    lName(event: any){
     this.userRelation.setLastName(event.target.value);
     this.isRequired=false;
   }
  
  
   //getting Date of Birth from user
   dob(event: any){
     this.userRelation.setDob(event.target.value);
     this.isRequired=false;
   }
  
  
   //getting relation with insurer from user
   onRelation(event:any){
     this.userRelation.setRelation(event.target.value);
     
     this.isRequired=false;
   }
  
  
   //getting nickname from user
   nickName(event: any){
     this.userRelation.setNickName(event.target.value);
   }
   
   
   formSubmit(f1: NgForm)
   {
     console.log(f1)
     if(this.instance1.isValid()){
       console.log(f1)
     }
   }
      onSubmit(f: NgForm) //method called on submitting the form through submit button
      {
         //getting the name of vehicle Brands through API call and storing it inside the instance of
         //ResponseVehicleBrand Pojo class
         console.log(this.instance)
         if(this.instance.isValid()){
          this.userPolicy.setUser_relation(this.userRelation);
          if(this.userPolicy.getExternal_entity_type().getExternalEntityTypeId()==1||
          this.userPolicy.getExternal_entity_type().getExternalEntityTypeId()==2)
        {  this.loading=true;
           this.vehicleDetails.getVehicleBrand(this.userPolicy.getExternal_entity_type().getExternalEntityTypeId()).subscribe((data:any)=>
         {
           
           this.responseVehicleBrand.setVehicle_brand(data.vehicle_brand);
           this.responseVehicleBrand.setClientId(data.clientId);
           this.responseVehicleBrand.setClientKey("f6b720f6707cd1c29b5bd3b5123f8115aeef13d8f34f9a757c0dca91c3e26af3");
           this.userPolicy.setUser_relation(this.userRelation);
           this.active = 2;
           //f.reset();
           this.selectTab(1);
         // this.instance1 = jQuery('.agentForm1').parsley();
          // this.userInsurance.getInsuranceName(this.name,this.userPolicy,this.responseEntityName,this.responseVehicleBrand);
           this.loading=false;
           //this.router.navigate(["user-insurance-details"]);
         }
         )}
    
         else
         {
          this.userPolicy.setUser_relation(this.userRelation);
          this.active = 2;
          //f.reset();
          this.selectTab(1);
        //  this.instance1 = jQuery('.agentForm1').parsley();
      //this.instance1 = jQuery('.agentForm').parsley();
         // this.userInsurance.getInsuranceName(this.name,this.userPolicy,this.responseEntityName,this.responseVehicleBrand);
          //this.router.navigate(["user-insurance-details"]);
         }
         }
      }
       //getting First name from user
      
     //2nd Tab
  
     externalNameId: String='';
     responseData: any;
     isMotor: boolean= false;
     carMakers:String[]=[];
    responseVehicle: any;
    vehicleBrandId: String;
    policyStart: any;
  
   fillRequired1() //method to check if required fields are entered or not
   {
  this.isRequired=true;
   }
  
  
  
     //this method is being used to get data from user-form component to this component
    getInsuranceName(insurance: InsuranceName,userPolicy: UserPolicy,
      responseEntityName:ResponseExternalEntityName,responseVehicleBrand: ResponseVehicleBrand)
      {    }
  
      onNameSelected(selectedValue:string) //on selecting the name of the insurance provider, this method gets triggered
      {
        //getting the externalEntityNameId of the insurance provder selected
        for(let event of this.responseData.external_entity_name.external_entity_name_list) 
        {
        if(event.entityName==selectedValue)
         {
            this.externalEntityName.setExternal_entity_name_id(event.externalEntityNameId);
            this.externalNameList.setEntityName(event.entityName);
            this.externalNameId=event.externalEntityNameId;
         }
        }
      }
       
      onVehicleBrand(event:any)// on selecting the vehicle brand (for car and two-wheeler) this method gets triggered
      {
        this.selectedValue=event.target.value;
        
        //getting the vehicle models list according to the brand selected through API call
        for(let entry of this.responseVehicleBrand.getVehicle_brand())
        {
          if(entry.brand==this.selectedValue)
          {
           this.userPolicyVehicleDetails.setBrand(this.selectedValue);
          
           this.vehicleBrandId=entry.vehicleBrandId;
           this.vehicleService.getVehicleType(this.vehicleBrandId).subscribe((data:any)=>
        {
          console.log(data)
          this.responseVehicleModel.setVehicle_model(data.vehicle_model);
          this.responseVehicleModel.setClientId(data.clientId);
          this.responseVehicleModel.setClientKey("f6b720f6707cd1c29b5bd3b5123f8115aeef13d8f34f9a757c0dca91c3e26af3");
          if(this.userPolicy.getExternal_entity_type().getExternalEntityTypeId()==1){}
        //  this.carInsurance.getModelDetails(this.responseVehicleModel,this.userPolicyVehicleDetails);
  
          else{}
       //   this.twowheelerInsurance.getModelDetails(this.responseVehicleModel,this.userPolicyVehicleDetails);
          })
        }
        
      }
    }
  
  
      noOfPeopleCovered(event:any) //getting no of people covered from user
      {
        this.userPolicy.setNoOfPeopleCovered(event.target.value);
        this.isRequired=false;
      }
  
  
      policyStartDate(event:any) //getting policy start date from user
      {
        this.userPolicy.setPolicyStartDate(event.target.value);
        this.policyStart=event.target.value;
        this.isRequired=false;
      }
  
  
      policyEndDate(event:any) //getting policy end date from user
      {
        this.userPolicy.setPolicyEndDate(event.target.value);
        this.isRequired=false;
      }
  
  
      policyNo(event:any) //getting policy number from user
      {
        this.userPolicy.setPolicyNumber(event.target.value);
        this.isRequired=false;
      }
  
  
      policyPremium(event:any) //getting policy premium from user
      {
        this.userPolicy.setPolicyPremium(event.target.value);
        this.isRequired=false;
      } 
  
  
      policyName(event:any) //getting policy name from user
      {
        this.userPolicy.setPolicyName(event.target.value);
        this.isRequired=false;
      } 
  
  
      agentName(event: any) //getting name of agent
      {
        this.userPolicyAgent.setName(event.target.value);
      }
  
  
      agentNo(event: any) //getting agent phone number
      {
        this.userPolicyAgent.setPhone(event.target.value);
      }
  
      
      //on submitting the form, it gets redirected to the component according to the insurance type selected
      onSubmit1(f: NgForm) 
      {
        console.log(f)
        if(this.instance1.isValid()){
          this.userPolicy.setUser_policy_agent(this.userPolicyAgent);
        this.userPolicy.setUser_policy_coverage(this.userPolicyCoverage);
        this.userPolicy.setExternal_entity_name(this.externalEntityName);
        //this.selectTab1(2);
        this.active = 3
        
        if(this.userPolicy.getExternal_entity_type().getExternalEntityTypeId()==6)
        {
          this.flagforTravel = true;
          this.selectTab1(2);
             //this.router.navigate(['travel-insurance']);
        }
        else if(this.userPolicy.getExternal_entity_type().getExternalEntityTypeId()==1)
        {
          this.flagforCar = true;
          this.selectTab1(2);
        }   
          //this.router.navigate(['car-insurance']);
      
  
        else if(this.userPolicy.getExternal_entity_type().getExternalEntityTypeId()==2)
        {
          this.flagforTwoWheeler = true;
          this.selectTab1(2);
            //this.router.navigate(['two-wheeler-insurance']);
        }
        else if(this.userPolicy.getExternal_entity_type().getExternalEntityTypeId()==12)
       {
         this.flagforWholeLife = true;
         this.selectTab1(2);
        }      //this.router.navigate(['whole-life-insurance']);
  
        else if(this.userPolicy.getExternal_entity_type().getExternalEntityTypeId()==4)
        {
          this.flagforULIP = true;
          this.selectTab1(2);
        }
        //this.router.navigate(['ULIP-insurance']);
  
        else if(this.userPolicy.getExternal_entity_type().getExternalEntityTypeId()==5)
        {
          this.flagforHealth = true;
          this.selectTab1(2);
        }
        //this.router.navigate(['health-insurance']);
  
        else if(this.userPolicy.getExternal_entity_type().getExternalEntityTypeId()==7)
        {
          this.flagforTermLife = true;
          this.selectTab1(2);
        }
        //this.router.navigate(['term-life-insurance']);
  
        else if(this.userPolicy.getExternal_entity_type().getExternalEntityTypeId()==8)
        {
          this.flagforEndowm = true;
          this.selectTab1(2);
        }
        //this.router.navigate(['endowment-insurance']);
  
        else if(this.userPolicy.getExternal_entity_type().getExternalEntityTypeId()==9)
        {
          this.flagforPersonalAccident = true;
          this.selectTab1(2);
        }
        //this.router.navigate(['personal-accident-insurance']);
  
        else if(this.userPolicy.getExternal_entity_type().getExternalEntityTypeId()==10)
        {
          this.flagforHome = true;
          this.selectTab1(2);
        }
        //this.router.navigate(['home-insurance']);
  
        else if(this.userPolicy.getExternal_entity_type().getExternalEntityTypeId()==11)
        {
          this.flagforRetirement = true;
          this.selectTab1(2);
        }
        //this.router.navigate(['retirement-insurance']);
      
        }
  
      
       }
      j: number;
  
      selectTab1(index: number): void {
        console.log(index)
        this.selectedIndex = index;
        this.isRequired = false;
        this.submitted = false;
        console.log(this.flagforCar)
        if(this.flagforTravel == true){
          this.getUserPolicy(this.userPolicy)
      {
       // this.url=this.userPolicy.getUser_policy_document().documentPath;  
       this.i=0;
       for(let event of this.userPolicy.getUser_policy_document()) 
       {
         this.urlArray[this.i]=event.documentPath;
         this.i++;
       }
       this.url=this.urlArray[0];
        this.urlType = this.urlArray[0].split(".").pop();
      }
        }
        if(this.flagforCar == true){
          this.userPolicyCoverage= new UserPolicyCoverage;
          this.getUserPolicy(this.userPolicy);
          this.userPolicy.setUser_policy_coverage(new UserPolicyCoverage);
        {
          
          {
            this.i=0;
            for(let event of this.userPolicy.getUser_policy_document()) 
            {
              this.urlArray[this.i]=event.documentPath;
              this.i++;
            }
            this.url=this.urlArray[0];
             this.urlType = this.urlArray[0].split(".").pop();
        }
        }
        
        this.getModelDetails(this.responseVehicleModel,this.userPolicyVehiclesDetails)
        {
          console.log("con",this.responseVehicleModel)
          this.i=0;
          for(let event of this.responseVehicleModel.vehicle_model)
          {
            this.carModels[this.i]=event.model;
            this.i++;
          }
        }
        }
        if( this.flagforTwoWheeler == true){
          this.userPolicyCoverage= new UserPolicyCoverage;
          this.getUserPolicy(this.userPolicy);
          this.userPolicy.setUser_policy_coverage(new UserPolicyCoverage);
         {
           
          // this.url=this.userPolicy.getUser_policy_document().documentPath;
         }
         this.getModelDetails(this.responseVehicleModel,this.userPolicyVehiclesDetails)
         {
          
           this.j=0;
           for(let event of this.userPolicy.getUser_policy_document()) 
           {
             this.urlArray[this.j]=event.documentPath;
             this.j++;
           }
           this.url=this.urlArray[0];
            this.urlType = this.urlArray[0].split(".").pop();
           this.i=0;
           for(let event of this.responseVehicleModel.vehicle_model)
           {
             this.carModels[this.i]=event.model;
             this.i++;
           }
         }
        }
        if(this.flagforWholeLife == true){
          this.getUserPolicy(this.userPolicy)
          {
            this.i=0;
            for(let event of this.userPolicy.getUser_policy_document()) 
            {
              this.urlArray[this.i]=event.documentPath;
              this.i++;
            }
            this.url=this.urlArray[0];
             this.urlType = this.urlArray[0].split(".").pop();
          }
        }
        if(this.flagforULIP == true){
          this.getUserPolicy(this.userPolicy)
          {
            {
              this.i=0;
              for(let event of this.userPolicy.getUser_policy_document()) 
              {
                this.urlArray[this.i]=event.documentPath;
                this.i++;
              }
              this.url=this.urlArray[0];
               this.urlType = this.urlArray[0].split(".").pop();
          }
          }
        }
        if(this.flagforTermLife == true){
          this.getUserPolicy(this.userPolicy)
          {
            this.j=0;
            for(let event of this.userPolicy.getUser_policy_document()) 
            {
              this.urlArray[this.j]=event.documentPath;
              this.j++;
            }
            this.url=this.urlArray[0];
             this.urlType = this.urlArray[0].split(".").pop();
          }
        }
        if(this.flagforPersonalAccident == true){
          this.getUserPolicy(this.userPolicy)
          {
            this.j=0;
            for(let event of this.userPolicy.getUser_policy_document()) 
            {
              this.urlArray[this.j]=event.documentPath;
              this.j++;
            }
            this.url=this.urlArray[0];
             this.urlType = this.urlArray[0].split(".").pop();
          }
        }
        if(this.flagforHome == true){
          this.getUserPolicy(this.userPolicy)
          {
            this.j=0;
            for(let event of this.userPolicy.getUser_policy_document()) 
            {
              this.urlArray[this.j]=event.documentPath;
              this.j++;
            }
            this.url=this.urlArray[0];
             this.urlType = this.urlArray[0].split(".").pop();
          }
        }
        if(this.flagforHealth == true){
          this.getUserPolicy(this.userPolicy)
          {
            this.j=0;
            for(let event of this.userPolicy.getUser_policy_document()) 
            {
              this.urlArray[this.j]=event.documentPath;
              this.j++;
            }
            this.url=this.urlArray[0];
             this.urlType = this.urlArray[0].split(".").pop();
          }
        }
        if(this.flagforEndowm == true){
          this.getUserPolicy(this.userPolicy)
          {
            this.j=0;
            for(let event of this.userPolicy.getUser_policy_document()) 
            {
              this.urlArray[this.j]=event.documentPath;
              this.j++;
            }
            this.url=this.urlArray[0];
             this.urlType = this.urlArray[0].split(".").pop();
          }
        }
        if(this.flagforRetirement == true){
          this.getUserPolicy(this.userPolicy)
          {
            this.j=0;
            for(let event of this.userPolicy.getUser_policy_document()) 
            {
              this.urlArray[this.j]=event.documentPath;
              this.j++;
            }
            this.url=this.urlArray[0];
             this.urlType = this.urlArray[0].split(".").pop();
          }
        }
      }
  
      fillRequired3()
   {
  this.isRequired=true;
   }
  
  
       
      getUserPolicy(userPolicy:UserPolicy){
  
      }
  
      baggageDelay(event:any)
    {
      if(event.target.value==="")
      this.userPolicyCoverage.setBaggageDelayCoverage(undefined);
      else
      this.userPolicyCoverage.setBaggageDelayCoverage(event.target.value);
    }
  
    emergencyMedical(event:any){
      if(event.target.value==="")
      this.userPolicyCoverage.setEmergencyMedicalEvacuationCoverage(undefined);
      else
      this.userPolicyCoverage.setEmergencyMedicalEvacuationCoverage(event.target.value);
    }
  
    baggageLoss(event:any)
    {
      if(event.target.value==="")
      this.userPolicyCoverage.setLossOfBaggageAndBelongingCoverage(undefined);
      else
      this.userPolicyCoverage.setLossOfBaggageAndBelongingCoverage(event.target.value);
    }
  
    overseasMedical(event:any)
    {
      if(event.target.value==="")
      this.userPolicyCoverage.setOverseasMedicalExpenseCoverage(undefined);
      else
      this.userPolicyCoverage.setOverseasMedicalExpenseCoverage(event.target.value);
    }
  
    deathSumAssured(event:any)
    {
      if(event.target.value==="")
      this.userPolicyCoverage.setDeathSumAssured(undefined);
      else
      this.userPolicyCoverage.setDeathSumAssured(event.target.value);
    }
  
    accidentalDeath(event:any)
    {
      if(event.target.value==="")
      this.userPolicyCoverage.setAccidentalDeathSumAssured(undefined);
      else
      this.userPolicyCoverage.setAccidentalDeathSumAssured(event.target.value);
    }
  
  
    disability(event:any)
    {
      if(event.target.value==="")
      this.userPolicyCoverage.setTotalAndPermanentDisabilitySumAssured(undefined);
      else
      this.userPolicyCoverage.setTotalAndPermanentDisabilitySumAssured(event.target.value);
    }
  
    travelDelay(event:any)
    {
      if(event.target.value==="")
      this.userPolicyCoverage.setTravelDelay(undefined);
      else
      this.userPolicyCoverage.setTravelDelay(event.target.value);
    }
  
    
    error: any;
    Success:string;
    submitted:boolean=false;
  
    onSubmit3(f: NgForm){
      if(this.instance2.isValid()){
  
        this.loading=true;
        this.userPolicy.setUser_policy_coverage(this.userPolicyCoverage);
        this.submitService.submitData(this.userPolicy).subscribe((data:any)=>
          {
            if(!data.successMessage)
            {this.error=data.errorMessage;
              Swal.fire({  
                icon: 'error',  
                title: 'Cancel',  
                text: this.error,    
              }) 
           // this.openSnackBar(this.error,"ok");
            }
            else
            {this.submitted=true;
            this.Success=data.successMessage;
            Swal.fire({  
              icon: 'success',  
              title: 'Success',  
              text: this.Success,    
            })
            this.tableflag = true
            
          this.router.navigate(['userpolicyupload/userdetailstable']);
          //  this.openSnackBar(this.Success,"ok");
            this.loading=false;}
          })
      }
    }
  
    fillRequired4()
   {
    this.isRequired=true;
  //this.openSnackBar("Please fill the required fields in Correct format!","ok");
  
   }
  
   goBack(){
    this.router.navigate(['userDetails']);
   }
  
  //  openSnackBar(message: string, action: string) {
  //   this.snackbar.open(message, action, {
  //      duration: 5000,
  //   });
  // }
  
  
  coverage:String[]=[];
  
   
  
    carModels:String[]=[];
  
  noClaimBonus(event:any){
    if(event.target.value==="")
    this.userPolicyCoverage.setNoClaimBonusInPercentage(undefined);
    else
    this.userPolicyCoverage.setNoClaimBonusInPercentage(event.target.value);
  
  }  
  
  insuredValue(event: any){
    if(event.target.value==="")
    this.userPolicyCoverage.setInsuredDeclared(undefined);
    else
    this.userPolicyCoverage.setInsuredDeclared(event.target.value);
  
  }
  
  excessAmount(event:any){
    if(event.target.value==="")
    this.userPolicyCoverage.setExcessOrDiscountAmount(undefined);
    else
    this.userPolicyCoverage.setExcessOrDiscountAmount(event.target.value);
  
  }
  
  registrationNo(event:any){
    
    this.userPolicyVehiclesDetails.setRegistrationNumber(event.target.value);
    
    }
    
    registrationYear(event:any){
         this.userPolicyVehiclesDetails.setRegistrationYear(event.target.value);
    
    }
    
    onVehicleModel(selectedValue:String){
    for(let event of this.responseVehicleModel.getVehicle_model())
    {
      if(event.model==selectedValue)
      {
        this.userPolicyVehiclesDetails.setModel(selectedValue);
      }
    }
    }
  
    isComprehensive(event:any){
      if(event.target.checked)
      {
        this.coverage.push("Comprehensive");
      }
      else
      {
        this.i=this.coverage.indexOf("Comprehensive");
        this.coverage.splice(this.i,1);
      }
    }
  
    isThirdParty(event:any){
      if(event.target.checked)
      {
        this.coverage.push("Third Party");
      }
      else
      {
        this.i=this.coverage.indexOf("Third Party");
        this.coverage.splice(this.i,1);
      }
    }
  
    isReplacement(event:any){
      if(event.target.checked)
      {
        this.coverage.push("Replacement Cover");
      }
      else
      {
        this.i=this.coverage.indexOf("Replacement Cover");
        this.coverage.splice(this.i,1);
      }
    }
  
  getModelDetails(data: ResponseVehicleModel,userPolicyVehiclesDetails:User_policy_vehicle_details){
  
  }
  
    
    onSubmit4(f: NgForm){
      if(this.instance5.isValid()){
        this.loading=true;
        if(this.coverage.length< 0)
        this.userPolicyCoverage.setCoverageType(this.coverage.join(","));
        this.userPolicy.setUser_policy_coverage(this.userPolicyCoverage);
        this.userPolicy.setUser_policy_vehicle_details(this.userPolicyVehiclesDetails);
        this.submitService.submitData(this.userPolicy).subscribe((data:any)=>
          {
            
            if(!data.successMessage)
            {this.error=data.errorMessage;
              Swal.fire({  
                icon: 'error',  
                title: 'Cancel',  
                text: this.error,    
              }) 
           // this.openSnackBar(this.error,"ok");
            this.loading=false;
            }
            else
            {this.submitted=true;
            this.Success=data.successMessage;
            Swal.fire({  
              icon: 'success',  
              title: 'Success',  
              text: this.Success,    
            })
            this.tableflag = true
            this.router.navigate(['userpolicyupload/userdetailstable']);
            //this.openSnackBar(this.Success,"ok");
            this.loading=false;}
          })
  
      }
    }
  
    fillRequired5()
   {
    this.isRequired=true;
  //this.openSnackBar("Please fill the required fields in Correct format!","ok");
  
   }
        
        onSubmit5(f: NgForm){
          if(this.instance2.isValid()){
            this.loading=true;
            if(this.coverage.length< 0)
            this.userPolicyCoverage.setCoverageType(this.coverage.join(","));
            this.userPolicy.setUser_policy_coverage(this.userPolicyCoverage);
            this.userPolicy.setUser_policy_vehicle_details(this.userPolicyVehiclesDetails);
            
            
            this.submitService.submitData(this.userPolicy).subscribe((data:any)=>
              {
                
                if(!data.successMessage)
            {this.error=data.errorMessage;
              Swal.fire({  
                icon: 'error',  
                title: 'Cancel',  
                text: this.error,    
              }) 
           // this.openSnackBar(this.error,"ok");
            }
            else
            {this.submitted=true;
            this.Success=data.successMessage;
            Swal.fire({  
              icon: 'success',  
              title: 'Success',  
              text: this.Success,    
            })
            this.tableflag = true
            this.router.navigate(['userpolicyupload/userdetailstable']);
            //this.openSnackBar(this.Success,"ok");
            this.loading=false;}
              })
  
          }
        }
  
        fillRequired6()
   {
  this.isRequired=true;
   }
  
  
  
    criticalIllness(event:any)
    {
      this.userPolicyCoverage.setCriticalIllnessSumAssured(event.target.value);
    }
  
  
    onSubmit6(f: NgForm){
      if(this.instance7.isValid()){
        this.loading=true;
        this.userPolicy.setUser_policy_coverage(this.userPolicyCoverage);
       
        
        this.submitService.submitData(this.userPolicy).subscribe((data:any)=>
          {
            if(!data.successMessage)
            {this.error=data.errorMessage;
              Swal.fire({  
                icon: 'error',  
                title: 'Cancel',  
                text: this.error,    
              }) 
            }
            else
            {this.submitted=true;
            this.Success=data.successMessage;
            Swal.fire({  
              icon: 'success',  
              title: 'Success',  
              text: this.Success,    
            })
            this.tableflag = true
            this.router.navigate(['userpolicyupload/userdetailstable']);
            this.loading=false;}
          })
  
      }
    }
  
    fillRequired7()
   {
  this.isRequired=true;
   }
  
    onSubmit7(f:NgForm){
      if(this.instance8.isValid()){
        this.loading=true;
        this.userPolicy.setUser_policy_coverage(this.userPolicyCoverage);
        
        this.submitService.submitData(this.userPolicy).subscribe((data:any)=>
          {
            if(!data.successMessage)
            {this.error=data.errorMessage;
              Swal.fire({  
                icon: 'error',  
                title: 'Cancel',  
                text: this.error,    
              }) 
            }
            else
            {this.submitted=true;
            this.Success=data.successMessage;
            Swal.fire({  
              icon: 'success',  
              title: 'Success',  
              text: this.Success,    
            })
            this.tableflag = true
          this.router.navigate(['userpolicyupload/userdetailstable']);
            this.loading=false;}
          })
  
      }
    }
  
  
  fillRequired8()
   {
  this.isRequired=true;
   }
  
    onSubmit8(f: NgForm){
      if(this.instance3.isValid()){
        this.loading=true;
        this.userPolicy.setUser_policy_coverage(this.userPolicyCoverage);
        this.submitted=true;
        this.submitService.submitData(this.userPolicy).subscribe((data:any)=>
          {
            
            if(!data.successMessage)
            {this.error=data.errorMessage;
              Swal.fire({  
                icon: 'error',  
                title: 'Cancel',  
                text: this.error,    
              }) 
            this.loading=false;
            }
            else
            {this.submitted=true;
            this.Success=data.successMessage;
            Swal.fire({  
              icon: 'success',  
              title: 'Success',  
              text: this.Success,    
            })
            this.tableflag = true
            this.router.navigate(['userpolicyupload/userdetailstable']);
            this.loading=false;}
          })
  
      }
    }
  
    fillRequired9()
   {
  this.isRequired=true;
   }
  
    temporaryDisablityTerm(event:any){
      if(event.target.value==="")
      this.userPolicyCoverage.setTemporaryTotalDisablementBenefitTerm(undefined);
      else
      this.userPolicyCoverage.setTemporaryTotalDisablementBenefitTerm(event.target.value);
    }
  
    temporaryDisablityPercent(event: any){
      if(event.target.value==="")
      this.userPolicyCoverage.setTemporaryTotalDisablementBenefit(undefined);
      else
      this.userPolicyCoverage.setTemporaryTotalDisablementBenefit(event.target.value);
    }
  
    occupationClass(selectedValue: String)
    {
      this.userPolicy.setOccupationClass(selectedValue);
    }
  
    accidentalHospitalAllowance(event:any){
      if(event.target.value==="")
      this.userPolicyCoverage.setAccidentalHospitalDailyAllowance(undefined);
      else
      this.userPolicyCoverage.setAccidentalHospitalDailyAllowance(event.target.value);
    }
  
  
  
  
    onSubmit9(f: NgForm){
      if(this.instance9.isValid()){
        this.loading=true;
        this.userPolicy.setUser_policy_coverage(this.userPolicyCoverage);
      
        this.submitService.submitData(this.userPolicy).subscribe((data:any)=>
          {
            
            if(!data.successMessage)
            {this.error=data.errorMessage;
            this.loading=false;
            Swal.fire({  
              icon: 'error',  
              title: 'Cancel',  
              text: this.error,    
            }) 
            }
            else
            {this.submitted=true;
            this.Success=data.successMessage;
            Swal.fire({  
              icon: 'success',  
              title: 'Success',  
              text: this.Success,    
            })
            this.tableflag = true
            this.router.navigate(['userpolicyupload/userdetailstable']);
            this.loading=false;}
          })
  
      }
    }
  
    fillRequired10()
   {
  this.isRequired=true;
   }
  
  
    alternateAccommodation(event: any){
      if(event.target.value==="")
      this.userPolicyCoverage.setAlternativeAccommodationSumAssured(undefined);
      else
      this.userPolicyCoverage.setAlternativeAccommodationSumAssured(event.target.value);
      this.isRequired=false;
    }
  
    householdContent(event:any){
      if(event.target.value==="")
      this.userPolicyCoverage.setHouseholdContentSumAssured(undefined);
      else
      this.userPolicyCoverage.setHouseholdContentSumAssured(event.target.value);
      this.isRequired=false;
    }
  
    renovation(event:any)
    {
      if(event.target.value==="")
      this.userPolicyCoverage.setRenovationSumAssured(undefined);
      else
      this.userPolicyCoverage.setRenovationSumAssured(event.target.value);
      this.isRequired=false;
    }
  
    onSubmit10(f: NgForm){
      if(this.instance4.isValid()){
        this.loading=true;
        this.userPolicy.setUser_policy_coverage(this.userPolicyCoverage);
        this.submitService.submitData(this.userPolicy).subscribe((data:any)=>
          {
            if(!data.successMessage)
            {this.error=data.errorMessage;
              Swal.fire({  
                icon: 'error',  
                title: 'Cancel',  
                text: this.error,    
              }) 
            this.loading=false;
            }
            else
            {this.submitted=true;
            this.Success=data.successMessage;
            Swal.fire({  
              icon: 'success',  
              title: 'Success',  
              text: this.Success,    
            })
            this.tableflag = true
            this.router.navigate(['userpolicyupload/userdetailstable']);
            this.loading=false;}
          })
      }
    }
  
    fillRequired11()
    {
   this.isRequired=true;
    }
   
     insuredAmount(event: any)
     {
       if(event.target.value==="")
       this.userPolicyCoverage.setInsuredOrCoverageAmount(undefined);
       else
       this.userPolicyCoverage.setInsuredOrCoverageAmount(event.target.value);
       this.isRequired=false;
     }
   
     preExistingDesease(selectedValue: String)
     {
       
       this.userPolicyCoverage.setPreExistingDisease(selectedValue);
       this.isRequired=false;
     }
   
     maternityBenefits(selectedValue: String)
     {
       this.userPolicyCoverage.setMaternityBenefits(selectedValue);
       this.isRequired=false;
     }
   
     onSubmit11(f:NgForm){
       if(this.instance10.isValid()){
        this.loading=true;
        this.userPolicy.setUser_policy_coverage(this.userPolicyCoverage);
        console.log(this.userPolicy);
        this.submitService.submitData(this.userPolicy).subscribe((data:any)=>
          {
            console.log(JSON.stringify(data));
            if(!data.successMessage)
            {this.error=data.errorMessage;
             Swal.fire({  
               icon: 'error',  
               title: 'Cancel',  
               text: this.error,    
             }) 
            this.loading=false;
            }
            else
            {this.submitted=true;
            this.Success=data.successMessage;
            Swal.fire({  
             icon: 'success',  
             title: 'Success',  
             text: this.Success,    
           })
           this.tableflag = true;
            this.router.navigate(['userpolicyupload/userdetailstable']);
            this.loading=false;}
          })
  
       }
     }
  
     fillRequired12()
   {
  this.isRequired=true;
   }
  
  
  
  
    maturityBenefit(event:any)
    {
      if(event.target.value==="")
      this.userPolicyCoverage.setLumpSumMaturityBenefit(undefined);
      else
      this.userPolicyCoverage.setLumpSumMaturityBenefit(event.target.value);
      this.isRequired=false;
    }
  
    maturityDate(event:any)
    {
      const date=new DatePipe('en-US').transform(event.target.value,"EEE, dd MMM yyyy");
      this.userPolicyCoverage.setMaturityDate(date);
      this.isRequired=false;
    }
  
    earlyCriticalIllness(event:any)
    {
      if(event.target.value==="")
      this.userPolicyCoverage.setEarlyCriticalIllnessSumAssured(undefined);
      else
      this.userPolicyCoverage.setEarlyCriticalIllnessSumAssured(event.target.value);
      this.isRequired=false;
    }
    onSubmit12(f:NgForm){
      if(this.instance11.isValid()){
       
      this.loading=true;
      this.userPolicy.setUser_policy_coverage(this.userPolicyCoverage);
      this.submitService.submitData(this.userPolicy).subscribe((data:any)=>
        {
          if(!data.successMessage)
          {this.error=data.errorMessage;
            Swal.fire({  
              icon: 'error',  
              title: 'Cancel',  
              text: this.error,    
            }) 
          this.loading=false;
          }
          else
          {this.submitted=true;
          this.Success=data.successMessage;
          Swal.fire({  
            icon: 'success',  
            title: 'Success',  
            text: this.Success,    
          })
          this.router.navigate(['userpolicyupload/userdetailstable']);
          this.loading=false;
        }
        }) 
      }
    }
  
    fillRequired13()
   {
  this.isRequired=true;
   }
  
  
  
    payoutFrequency(selectedValue: any)
    {
      this.userPolicyCoverage.setPayOutFrequency(selectedValue);
    }
  
    payoutAmount(event: any)
    {
      if(event.target.value==="")
      this.userPolicyCoverage.setPayOutAmount(undefined);
      else
      this.userPolicyCoverage.setPayOutAmount(event.target.value);
    }
  
    onSubmit13(f:NgForm){
      if(this.instance12.isValid()){
  
        this.loading=true;
        this.userPolicy.setUser_policy_coverage(this.userPolicyCoverage);
      
        this.submitService.submitData(this.userPolicy).subscribe((data:any)=>
          {
            
            if(!data.successMessage)
            {this.error=data.errorMessage;
              Swal.fire({  
                icon: 'error',  
                title: 'Cancel',  
                text: this.error,    
              }) 
            this.loading=false;
            }
            else
            {this.submitted=true;
            this.Success=data.successMessage;
            Swal.fire({  
              icon: 'success',  
              title: 'Success',  
              text: this.Success,    
            })
            this.tableflag = true
            this.router.navigate(['userpolicyupload/userdetailstable']);
            this.loading=false;}
          })
      }
    }
    constructor(
      private modalService: NgbModal,
      private service:RequestUserDocumentService,private responseUserDocument:ResponseUserDocument,
      private userPolicy:UserPolicy,private user:User,private external_entity_type:ExternalEntityType,
      private external_entity_name:External_entity_name,private responseLogin:ResponseLogin,
      private loginComponent:LoginMobilenoComponent,
      private router: Router,
      private relation:Relation,
      private userPolicyDocument:User_policy_document,
      private submitService:SubmitService,
      private responseEntityName:ResponseExternalEntityName,
      private name: InsuranceName,
      private userRelation:User_relation,
      private vehicleDetails:VehicleDetailsService,
      private responseVehicleBrand:ResponseVehicleBrand,
      //private formBuilder: FormBuilder,
      private relations:Relation,
      private externalNameList:external_entity_name_list,
      private userPolicyCoverage:UserPolicyCoverage,
      private userPolicyAgent:UserPolicyAgent,
      private externalEntityName:External_entity_name,
      private userPolicyVehicleDetails:User_policy_vehicle_details,
      private vehicleService:VehicleDetailsService,
      private responseVehicleModel: ResponseVehicleModel,
      private userPolicyVehiclesDetails: User_policy_vehicle_details, private renderer: Renderer2, private dataService:DataService
      ) {
        
       }
  
       
  
    ngOnInit() {
      this.instance = jQuery('.agentForm').parsley();
      this.instance1 = jQuery('.agentForm1').parsley();
      this.instance2 = jQuery('.agentForm2').parsley();
      this.instance3 = jQuery('.agentForm3').parsley();
      this.instance4 = jQuery('.agentForm4').parsley();
      this.instance5 = jQuery('.agentForm5').parsley();
      this.instance6 = jQuery('.agentForm6').parsley();
      this.instance7 = jQuery('.agentForm7').parsley();
      this.instance8 = jQuery('.agentForm8').parsley();
      this.instance9 = jQuery('.agentForm9').parsley();
      this.instance10 = jQuery('.agentForm10').parsley();
      this.instance11 = jQuery('.agentForm11').parsley();
      this.instance12 = jQuery('.agentForm12').parsley();
      this.pageSettings = {
        pageSidebarMinified: false,
        pageContentFullHeight: false,
        pageContentFullWidth: false,
        pageContentInverseMode: false,
        pageWithFooter: false,
        pageWithoutSidebar: false,
        pageSidebarRight: false,
        pageSidebarRightCollapsed: false,
        pageSidebarTwo: false,
        pageSidebarWide: false,
        pageSidebarTransparent: false,
        pageSidebarLight: false,
        pageTopMenu: false,
        pageEmpty: false,
        pageBodyWhite: false,
        pageMobileSidebarToggled: false,
        pageMobileSidebarFirstClicked: false,
        pageMobileSidebarRightToggled: false,
        pageMobileSidebarRightFirstClicked: false
      };
     this.urlformodel = sessionStorage.getItem("urlformodel")
      //this.loading=true;
      //window.history.pushState( {} , 'userDetails','#/userDetails');
        //this.loading=true;
          //getting the insurance name from the user-form component according to the insurance type selected
          
          
  
          // this.dataService.currentMessage.subscribe(message => {
          //   console.log(message)
          //   this.backfunc();  
          // })
    }
  
  
    
    // window scroll
    pageHasScroll;
    @HostListener('window:scroll', ['$event'])
    onWindowScroll($event) {
      var doc = document.documentElement;
      var top = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);
      if (top > 0) {
        this.pageHasScroll = true;
      } else {
        this.pageHasScroll = false;
      }
    }
  
    // clear settings to default
    clearSettings() {
      this.pageSettings.pageSidebarMinified = false;
      this.pageSettings.pageContentFullHeight = false,
      this.pageSettings.pageContentFullWidth = false;
      this.pageSettings.pageWithFooter = false;
      this.pageSettings.pageWithoutSidebar = false;
      this.pageSettings.pageSidebarRight = false;
      this.pageSettings.pageSidebarRightCollapsed = false;
      this.pageSettings.pageSidebarTwo = false;
      this.pageSettings.pageSidebarWide = false;
      this.pageSettings.pageSidebarTransparent = false;
      this.pageSettings.pageSidebarLight = false;
      this.pageSettings.pageTopMenu = false;
      this.pageSettings.pageEmpty = false;
      this.pageSettings.pageBodyWhite = false;
      this.pageSettings.pageContentInverseMode = false;
      this.pageSettings.pageMobileSidebarToggled = false;
      this.pageSettings.pageMobileSidebarFirstClicked = false;
      this.pageSettings.pageMobileRightSidebarToggled = false;
      this.pageSettings.pageMobileRightSidebarFirstClicked = false;
      this.renderer.removeClass(document.body, 'bg-white');
    }
  
    // set page settings
    setPageSettings(settings) {
      for (let option in settings) {
        this.pageSettings[option] = settings[option];
        if (option == 'pageBodyWhite' && settings[option] == true) {
          this.renderer.addClass(document.body, 'bg-white');
        }
      }
    }
  
    // set page minified
    onToggleSidebarMinified(val: boolean):void {
      if (this.pageSettings.pageSidebarMinified) {
        this.pageSettings.pageSidebarMinified = false;
      } else {
        this.pageSettings.pageSidebarMinified = true;
      }
    }
  
    // set page right collapse
    onToggleSidebarRight(val: boolean):void {
      if (this.pageSettings.pageSidebarRightCollapsed) {
        this.pageSettings.pageSidebarRightCollapsed = false;
      } else {
        this.pageSettings.pageSidebarRightCollapsed = true;
      }
    }
  
    // hide mobile sidebar
    onHideMobileSidebar(val: boolean):void {
      if (this.pageSettings.pageMobileSidebarToggled) {
        if (this.pageSettings.pageMobileSidebarFirstClicked) {
          this.pageSettings.pageMobileSidebarFirstClicked = false;
        } else {
          this.pageSettings.pageMobileSidebarToggled = false;
        }
      }
    }
  
    // toggle mobile sidebar
    onToggleMobileSidebar(val: boolean):void {
      if (this.pageSettings.pageMobileSidebarToggled) {
        this.pageSettings.pageMobileSidebarToggled = false;
      } else {
        this.pageSettings.pageMobileSidebarToggled = true;
        this.pageSettings.pageMobileSidebarFirstClicked = true;
      }
    }
  
  
    // hide right mobile sidebar
    onHideMobileRightSidebar(val: boolean):void {
      if (this.pageSettings.pageMobileRightSidebarToggled) {
        if (this.pageSettings.pageMobileRightSidebarFirstClicked) {
          this.pageSettings.pageMobileRightSidebarFirstClicked = false;
        } else {
          this.pageSettings.pageMobileRightSidebarToggled = false;
        }
      }
    }
  
    // toggle right mobile sidebar
    onToggleMobileRightSidebar(val: boolean):void {
      if (this.pageSettings.pageMobileRightSidebarToggled) {
        this.pageSettings.pageMobileRightSidebarToggled = false;
      } else {
        this.pageSettings.pageMobileRightSidebarToggled = true;
        this.pageSettings.pageMobileRightSidebarFirstClicked = true;
      }
    }
  
    

 
 }

