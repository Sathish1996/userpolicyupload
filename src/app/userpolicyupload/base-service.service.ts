import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BaseServiceService {
  
  constructor() { }

  //key for hashing password
  key:string="F1NS@ll@2o19w6";
  baseUrl():string
  {
    //baseURL for running locally in Finsall.co.in "https://cors-anywhere.herokuapp.com/https://www.finsall.co.in/FinsAllServer/api";

    //baseURL for running in server in Finsall.co.in= 'https://www.finsall.co.in/FinsAllServer/api';

    //baseURL for running locally in Contavo "https://cors-anywhere.herokuapp.com/http://213.136.87.68:8080/FinsAllServer/api"

    //baseURL for running in server in Contavo "http://213.136.87.68:8080/FinsAllServer/api"
  
 // return  "https://cors-anywhere.herokuapp.com/https://www.finsall.co.in/FinsAllServer/api";
 
  //-------------------Code to run in Finsall.co.in--------------------//

//   var URL=window.location.href;
//   console.log(URL);
// var indexOfValue = URL.indexOf("www");
// if(indexOfValue > 0)
// var SERVER_URL = 'https://www.finsall.com/FinsAllServer/api';
// else
// var SERVER_URL = 'https://finsall.com/FinsAllServer/api';  
//   return SERVER_URL;

  //-------------------------------------------------------------------//

 // return "http://213.136.87.68:8080/FinsAllServer/api";
  
   return "https://cors-anywhere.herokuapp.com/http://213.136.87.68:8080/FinsAllServer/api";
  }
}
