import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoginMobilenoComponent } from './login-mobileno/login-mobileno.component';
import { RequestDocument } from './model/RequestDocument';
import { BaseServiceService } from './base-service.service';

@Injectable({
  providedIn: 'root'
})
export class RequestUserDocumentService {

  element: any;
 
 
  private options = { headers: new HttpHeaders().set('Content-Type', 'application/json')
  .set('authentication-token',sessionStorage.getItem("password"))
  .set('authentication-username',sessionStorage.getItem("userId"))
   };
 
   setElement(data:any){
     this.element=data;
   }

   getElement():any{
    return this.element;
   }

   getUserDocument()
  {

   this.request.setServiceName("FinsAllService");
   this.request.setServiceMethod("getUserPolicyDocumentDetail");
   
  this.request.setClientKey("f6b720f6707cd1c29b5bd3b5123f8115aeef13d8f34f9a757c0dca91c3e26af3");
   this.request.setClientId(sessionStorage.getItem("clientId"));
   this.request.setVersion(this.loginMobilenoComponent.sendResponseLogin().getVersion());
   this.request.setLoggedInUserId(this.loginMobilenoComponent.sendResponseLogin().getUserId());
   
   return this.http.post(this.baseService.baseUrl(),JSON.stringify(this.request),this.options).toPromise();
   
  }

  deactivateDocument(documentId:number)
  {
    this.request.setServiceName("FinsAllService");
    this.request.setServiceMethod("updateUserDocument");
    this.request.setClientKey("f6b720f6707cd1c29b5bd3b5123f8115aeef13d8f34f9a757c0dca91c3e26af3");
   this.request.setClientId(sessionStorage.getItem("clientId"));
   this.request.setVersion(this.loginMobilenoComponent.sendResponseLogin().getVersion());
   this.request.setLoggedInUserId(this.loginMobilenoComponent.sendResponseLogin().getUserId());
   this.request.setGenericIdentifier(documentId);
   return this.http.post(this.baseService.baseUrl(),JSON.stringify(this.request),this.options).toPromise();
  }

  constructor(private http: HttpClient,private request:RequestDocument,
    private loginMobilenoComponent: LoginMobilenoComponent,private baseService: BaseServiceService) { }
}
