import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserpolicyuploadRoutingModule } from './userpolicyupload-routing.module';
import { LoginMobilenoComponent } from './login-mobileno/login-mobileno.component';
import { UserDetailsTableComponent } from './user-details-table/user-details-table.component';
import { SidebarComponent  } from './sidebar/sidebar.component';
import { HeaderComponent } from './header/header.component';
import { NgSlimScrollModule }   from 'ngx-slimscroll';
import {MatPaginatorModule} from '@angular/material/paginator';
    import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSelectModule} from '@angular/material/select';
import {MatTabsModule} from '@angular/material/tabs';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { from } from 'rxjs';
import { ValidateLoginService } from './validate-login.service';
import { BaseServiceService } from './base-service.service';
import { AuthGuardService } from './auth-guard.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ResponseLogin } from './model/ResponseLogin';
import { RequestLogin } from './model/RequestLogin';
import { HttpClientModule } from '@angular/common/http';
import { ResponseUserDocument } from './model/ResponseUserDocument';
import { RequestUserDocumentService } from './request-user-document.service';
import { UserPolicy } from './model/UserPolicy';
import { User } from './model/user';
import { ExternalEntityType } from './model/External_entity_type';
import { External_entity_name } from './model/External_entity_name';
import { Relation } from './model/Relation';
import { User_policy_document } from './model/user_policy_document';
import { RequestDocument } from './model/RequestDocument';
import { SubmitService } from './submit.service';
import { RequestRelation } from './model/RequestRelation';
import { VehicleDetailsService } from './vehicle-details.service';
import { InsuranceName } from './insuranceName';
import { RequestVehicleModel } from './model/RequestVehicleModel';
import { RequestVehicleBrand } from './model/RequestVehicleBrand';
import { ResponseExternalEntityName } from './model/ResponseExternalEntityName';
import { User_relation } from './model/User_relation';
import { ResponseVehicleBrand } from './model/ResponseVehicleBrand';
import { external_entity_name_list } from './model/External_Entity_Name_List';
import { UserPolicyCoverage } from './model/User_policy_coverage';
import { UserPolicyAgent } from './model/User_policy_agent';
import { User_policy_vehicle_details } from './model/User_policy_vehicle_details';
import { ResponseVehicleModel } from './model/ResponseVehicleModel';
import { NgxImageZoomModule } from 'ngx-image-zoom';
import { PinchZoomModule } from 'ngx-pinch-zoom';
import { DataService } from './dataServicefortoggle';
import { UserFormComponent } from './user-form/user-form.component';


@NgModule({
  declarations: [
    LoginMobilenoComponent,
    UserDetailsTableComponent,
    HeaderComponent,
    SidebarComponent,
    UserFormComponent
  ],
  imports: [
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    UserpolicyuploadRoutingModule,
    NgSlimScrollModule,
    MatTableModule, 
    MatIconModule, 
    MatButtonModule, 
    MatDialogModule,
    MatSnackBarModule,
    MatInputModule, 
    MatFormFieldModule, 
    MatDatepickerModule,
    MatSelectModule ,
    MatPaginatorModule,
    NgbModule,
    MatTabsModule,
    NgxImageZoomModule,
    PinchZoomModule
  ],
  providers: [
    LoginMobilenoComponent,
    ValidateLoginService,
    AuthGuardService,
    BaseServiceService,
    ResponseLogin,
    RequestLogin,
    RequestUserDocumentService,
    ResponseUserDocument,
    UserPolicy,
    User,
    ExternalEntityType,
    External_entity_name,
    Relation,
    User_policy_document,
    RequestDocument,
    SubmitService,
    RequestRelation,
    VehicleDetailsService,
    InsuranceName,
    RequestVehicleModel,
    RequestVehicleBrand,
    ResponseExternalEntityName,
    User_relation,
    ResponseVehicleBrand,
    external_entity_name_list,
    UserPolicyCoverage,
    UserPolicyAgent,
    User_policy_vehicle_details,
    ResponseVehicleModel,
    DataService
  ],
})
export class UserpolicyuploadModule { }
