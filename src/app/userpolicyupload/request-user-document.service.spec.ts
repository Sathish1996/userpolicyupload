import { TestBed } from '@angular/core/testing';

import { RequestUserDocumentService } from './request-user-document.service';

describe('RequestUserDocumentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RequestUserDocumentService = TestBed.get(RequestUserDocumentService);
    expect(service).toBeTruthy();
  });
});
