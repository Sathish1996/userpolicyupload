import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginMobilenoComponent } from './login-mobileno/login-mobileno.component';
import { ResponseLogin } from './model/ResponseLogin';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(
        // declare variables
        private responseLogin:ResponseLogin,
        private router:Router
  ) {}

    canActivate(
     route: ActivatedRouteSnapshot,
     state: RouterStateSnapshot,

  ): boolean {
        // logic that determines true or false
        var userId = '';
        userId = sessionStorage.getItem("userId");
          if(userId != '' && userId == undefined)
          return true;
          else
          {
           this.router.navigate(["userpolicyupload/login"]);
            return false;
          }
          
  }
}