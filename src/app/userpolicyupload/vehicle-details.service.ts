import { Injectable } from '@angular/core';

import { HttpHeaders,HttpClient } from '@angular/common/http';
import { RequestVehicleBrand } from './model/RequestVehicleBrand';
import { LoginMobilenoComponent } from './login-mobileno/login-mobileno.component';
import { RequestVehicleModel } from './model/RequestVehicleModel';
import { BaseServiceService } from './base-service.service';

@Injectable({
  providedIn: 'root'
})
export class VehicleDetailsService {

  constructor(private http: HttpClient,private requestBrand: RequestVehicleBrand,
    private loginMobilenoComponent: LoginMobilenoComponent,
    private requestModel:RequestVehicleModel,private baseService: BaseServiceService) { }

  //baseURL: string = "https://cors-anywhere.herokuapp.com/https://www.finsall.co.in/FinsAllServer/api";

  //baseURL: string = 'https://www.finsall.co.in/FinsAllServer/api';

  
  private options = { headers: new HttpHeaders().set('Content-Type', 'application/json')
  .set('authentication-token',sessionStorage.getItem("password"))
  .set('authentication-username',sessionStorage.getItem("userId"))
   };

   getVehicleBrand(identifier: number)
   {
    this.requestBrand.setClientId(sessionStorage.getItem("clientId"));
    this.requestBrand.setClientKey("f6b720f6707cd1c29b5bd3b5123f8115aeef13d8f34f9a757c0dca91c3e26af3");
    this.requestBrand.setLoggedInUserId(this.loginMobilenoComponent.sendResponseLogin().getUserId());
    this.requestBrand.setVersion(this.loginMobilenoComponent.sendResponseLogin().getVersion());
    this.requestBrand.setServiceName("FinsAllService");
    this.requestBrand.setServiceMethod("getVehicleBrandsById");
    this.requestBrand.setGenericIdentifier(identifier);
    return this.http.post(this.baseService.baseUrl(),JSON.stringify(this.requestBrand),this.options);
   }

   getVehicleType(brandId: String)
   {
    this.requestModel.setClientId(sessionStorage.getItem("clientId"));
    
    this.requestModel.setClientKey("f6b720f6707cd1c29b5bd3b5123f8115aeef13d8f34f9a757c0dca91c3e26af3");
    this.requestModel.setLoggedInUserId(this.loginMobilenoComponent.sendResponseLogin().getUserId());
    this.requestModel.setServiceMethod("getVehicleModelsByBrandId");
    this.requestModel.setServiceName("FinsAllService");
    this.requestModel.setVersion(this.loginMobilenoComponent.sendResponseLogin().getVersion());
    this.requestModel.setGenericIdentifier(brandId);
    return this.http.post(this.baseService.baseUrl(),JSON.stringify(this.requestModel),this.options);

   }

  }

