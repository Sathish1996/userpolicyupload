import { group, animate, query, style, trigger, transition, state } from '@angular/animations';
import { Component, Input, Output, EventEmitter, ElementRef, HostListener } 		 from '@angular/core';
import * as global 			 from '../globals';
import {  NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import { DataService } from '../dataServicefortoggle';
import { from } from 'rxjs';

@Component({
  selector: 'sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
  animations: [
    trigger('expandCollapse', [
      state('expand', style({ height: '*', overflow: 'hidden', display: 'block' })),
      state('collapse', style({ height: '0px', overflow: 'hidden', display: 'block' })),
      state('active', style({ height: '*', overflow: 'hidden', display: 'block' })),
      transition('expand <=> collapse', animate(100)),
      transition('active => collapse', animate(100))
    ])
  ]
})

export class SidebarComponent {
	closeResult: string;
  navProfileState = 'collapse';
  firstname: any;
  lastName: any;
  role: any;
	slimScrollOptions = global.whiteSlimScrollOptions;
	@Output() toggleSidebarMinified = new EventEmitter<boolean>();
	@Output() hideMobileSidebar = new EventEmitter<boolean>();
	@Input() pageSidebarTransparent;

  toggleNavProfile() {
    if (this.navProfileState == 'collapse') {
      this.navProfileState = 'expand';
    } else {
      this.navProfileState = 'collapse';
    }
  }

	toggleMinified() {
    console.log("coming")
		this.toggleSidebarMinified.emit(true);
	}

	expandCollapseSubmenu(currentMenu, allMenu, active) {
		for (let menu of allMenu) {
			if (menu != currentMenu) {
				menu.state = 'collapse';
			}
		}
		if (currentMenu.state == 'expand' || (active.isActive && !currentMenu.state)) {
			currentMenu.state = 'collapse';
		} else {
			currentMenu.state = 'expand';
		}
	}

	@HostListener('document:click', ['$event'])
  clickout(event) {
    if(!this.eRef.nativeElement.contains(event.target)) {
		  this.hideMobileSidebar.emit(true);
    }
  }

  constructor(private eRef: ElementRef,
    private modalService: NgbModal,private router: Router,private dataService:DataService) {
      this.firstname = sessionStorage.getItem("firstName");
      this.lastName = sessionStorage.getItem("lastName");
      this.role = sessionStorage.getItem("roles")
  }

  menus = [{
		'icon': 'fa fa-calendar',
		'title': 'Home',
		'url': 'userpolicyupload/userdetailstable'
	},{
		'icon': 'fa fa-map',
		'title': 'Logout',
		'url': 'logout'
	}];

	

	modalReference: NgbModalRef;

  open11(){
    window.location.href = '/userpolicyupload/userdetailstable'
  //this.router.navigate(['userpolicyupload/userdetailstable']);
   // this.dataService.changeMessage("mainpage");
  }

open1(content) {
  this.modalReference = this.modalService.open(content);
  this.modalReference.result.then((result) => {
    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
  }
  
  
  JoinAndClose() {
    this.router.navigate(["userpolicyupload/login"]);
    this.modalReference.close();
  }

  JoinAndClose1() {
    this.modalReference.close();
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}
