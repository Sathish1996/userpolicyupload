import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginMobilenoComponent } from './login-mobileno.component';

describe('LoginMobilenoComponent', () => {
  let component: LoginMobilenoComponent;
  let fixture: ComponentFixture<LoginMobilenoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginMobilenoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginMobilenoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
