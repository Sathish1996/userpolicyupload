import { Component, OnInit, HostListener } from '@angular/core';
import {Router} from '@angular/router';
import { delay } from 'rxjs/operators';
import { LocationStrategy } from '@angular/common';
import { ValidateLoginService } from '../validate-login.service';
import { ResponseLogin } from '../model/ResponseLogin';
import { AuthGuardService } from '../auth-guard.service';
//import { sha256} from 'js-sha256';
import { BaseServiceService } from '../base-service.service';
@Component({
  selector: 'app-login-mobileno',
  templateUrl: './login-mobileno.component.html',
  styleUrls: ['./login-mobileno.component.css']
})
//this component is for login using User registered mobile number and password
export class LoginMobilenoComponent implements OnInit {
  mobileNo: String= "";
  password: string= "";
  isValid: boolean= false; 
  clientId: string="2017";
  isRequired: boolean=false;
  errorMessage:String='';
  isLoggedOut: boolean=false;
  loading = false;
  key:string= this.baseService.key;
 
  fillRequired()
  {
 this.isRequired=true;
  }
   
  @HostListener('window:popstate', ['$event'])
   onPopState(event:any) {
     this.router.navigate(["login"]);
     
   }
 
  onEnteringMobileNo(event: any)
  {
    this.mobileNo=event.target.value;
    
    this.isRequired=false;
  }
 
  onEnteringPassword(event: any)
  {
    this.password=event.target.value;
    this.isRequired=false;
  //  this.password=(sha256.hmac(this.key, this.password));
  }
 
  check(){
          //this.response=this.validate.validateWithMobileNo(this.mobileNo,this.password);
          this.loading=true;
          this.validate.validateWithMobileNo(this.mobileNo,"6944d9e7d2c3b3e92224106a51def3d496cc9c3244e84103cc6edf918111ec4e").subscribe((data:any) => 
       {
         console.log(data)
         
         this.response.setClientId((data.clientId));
         sessionStorage.setItem('clientId', data.clientId);
         this.response.setClientKey("f6b720f6707cd1c29b5bd3b5123f8115aeef13d8f34f9a757c0dca91c3e26af3");
         this.response.setPassword((data.password));
         sessionStorage.setItem('password', data.password);
         this.response.setVersion((data.version));
         sessionStorage.setItem('version', data.version);
         this.response.setUserId((data.userId));
         sessionStorage.setItem('userId', data.userId);
         this.response.setUserName((data.userName));
         this.response.setLastName((data.lastName));
         sessionStorage.setItem('lastName', data.lastName);
         this.response.setFirstName((data.firstName));
         sessionStorage.setItem('firstName', data.firstName);
         sessionStorage.setItem('roles', data.roles);
         this.response.setEmailId((data.emailId));
         this.response.setMobileNo((data.mobileNo));
         this.response.setPan((data.pan));
         this.response.setRoleId((data.roleId));
         this.response.setCountry((data.country));
         this.response.setErrorMessage((data.errorMessage));
 
         this.loading=false;
         
         if (this.response.getErrorMessage()==undefined)
      {
        
       this.router.navigate(['userpolicyupload/userdetailstable']);
       }
       else{
         console.log("incorrect credentials");
         this.errorMessage=this.response.getErrorMessage();
         
         this.isValid=true;
       }
       
      } )
     
      
         }
 
   sendResponseLogin(){
     return this.response;
   }
  getResponseLogin(res:ResponseLogin){
 
   this.response=res;
   this.isLoggedOut=true;
  }
 
  preventBackButton() {
   history.pushState(null, null, location.href);
   this.locationStrategy.onPopState(() => {
     history.pushState(null, null, location.href);
   })
 }
   constructor(private validate: ValidateLoginService,
     private response: ResponseLogin, private router:Router,
     private auth:AuthGuardService,
     private locationStrategy: LocationStrategy,
     private baseService:BaseServiceService) { }
 
   ngOnInit() {
     
   }
 
 }

