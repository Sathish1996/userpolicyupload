import { InsuranceType } from './InsuranceType';
import { InsuranceTypeRoot } from './InsuranceTypeRoot';

export class InsuranceTypeAndRoot{
    private insuranceTypeAndRootId: number;
	private  insuranceType:InsuranceType;
	
	private  insuranceTypeRoot:InsuranceTypeRoot;

    public getInsuranceTypeAndRootId(): number {
        return this.insuranceTypeAndRootId;
    }

    public setInsuranceTypeAndRootId(insuranceTypeAndRootId: number): void {
        this.insuranceTypeAndRootId = insuranceTypeAndRootId;
    }

    public getInsuranceType(): InsuranceType {
        return this.insuranceType;
    }

    public setInsuranceType(insuranceType: InsuranceType): void {
        this.insuranceType = insuranceType;
    }

    public getInsuranceTypeRoot(): InsuranceTypeRoot {
        return this.insuranceTypeRoot;
    }

    public setInsuranceTypeRoot(insuranceTypeRoot: InsuranceTypeRoot): void {
        this.insuranceTypeRoot = insuranceTypeRoot;
    }

}