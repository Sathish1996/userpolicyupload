import { Relationship_to_insured } from './Relationship_to_insured';

export class Relation
{
    private clientId: String;

    private version:String ;

    private relationship_to_insured:Relationship_to_insured[];

    private errorMessage:String;

    

    public getErrorMessage(): String {
        return this.errorMessage;
    }

    public setErrorMessage(errorMessage: String): void {
        this.errorMessage = errorMessage;
    }


    public getClientId(): String {
        return this.clientId;
    }

    public setClientId(clientId: String): void {
        this.clientId = clientId;
    }

    public getVersion(): String {
        return this.version;
    }

    public setVersion(version: String): void {
        this.version = version;
    }

    public getRelationship_to_insured(): Relationship_to_insured[] {
        return this.relationship_to_insured;
    }

    public setRelationship_to_insured(relationship_to_insured: Relationship_to_insured[]): void {
        this.relationship_to_insured = relationship_to_insured;
    }


}
