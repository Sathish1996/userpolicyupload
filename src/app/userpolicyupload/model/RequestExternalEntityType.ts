export class RequestExternalEntityType{
    private serviceName: String;
    private serviceMethod: String;
    private loggedInUserId: String;
    private clientId: String;
    private version: String;
    private clientKey: string;

    public getClientKey(): string {
        return this.clientKey;
    }

    public setClientKey(clientKey: string): void {
        this.clientKey = clientKey;
    }


    public getServiceName(): String {
        return this.serviceName;
    }

    public setServiceName(serviceName: String): void {
        this.serviceName = serviceName;
    }

    public getServiceMethod(): String {
        return this.serviceMethod;
    }

    public setServiceMethod(serviceMethod: String): void {
        this.serviceMethod = serviceMethod;
    }

    public getLoggedInUserId(): String {
        return this.loggedInUserId;
    }

    public setLoggedInUserId(loggedInUserId: String): void {
        this.loggedInUserId = loggedInUserId;
    }

    public getClientId(): String {
        return this.clientId;
    }

    public setClientId(clientId: String): void {
        this.clientId = clientId;
    }

    public getVersion(): String {
        return this.version;
    }

    public setVersion(version: String): void {
        this.version = version;
    }

}