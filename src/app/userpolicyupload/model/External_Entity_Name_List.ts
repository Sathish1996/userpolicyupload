export class external_entity_name_list
{
    private website: String;

    public externalEntityNameId: String;

    public entityName: String;

    private icon: String;

    private telephone: String;

    public getWebsite(): String {
        return this.website;
    }

    public setWebsite(website: String): void {
        this.website = website;
    }

    public getExternalEntityNameId(): String {
        return this.externalEntityNameId;
    }

    public setExternalEntityNameId(externalEntityNameId: String): void {
        this.externalEntityNameId = externalEntityNameId;
    }

    public getEntityName(): String {
        return this.entityName;
    }

    public setEntityName(entityName: String): void {
        this.entityName = entityName;
    }

    public getIcon(): String {
        return this.icon;
    }

    public setIcon(icon: String): void {
        this.icon = icon;
    }

    public getTelephone(): String {
        return this.telephone;
    }

    public setTelephone(telephone: String): void {
        this.telephone = telephone;
    }

}