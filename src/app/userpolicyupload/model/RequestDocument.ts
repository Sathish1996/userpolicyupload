export class RequestDocument{

    private clientId: String;

    private loggedInUserId: String;

    private serviceName: String;

    private version: String;

    private genericIdentifier: number;

    private serviceMethod: String;

    private clientKey: string;

    public getClientKey(): string {
        return this.clientKey;
    }

    public setClientKey(clientKey: string): void {
        this.clientKey = clientKey;
    }


    public getClientId(): String {
        return this.clientId;
    }

    public setClientId(clientId: String): void {
        this.clientId = clientId;
    }

    public getLoggedInUserId(): String {
        return this.loggedInUserId;
    }

    public setLoggedInUserId(loggedInUserId: String): void {
        this.loggedInUserId = loggedInUserId;
    }

    public getServiceName(): String {
        return this.serviceName;
    }

    public setServiceName(serviceName: String): void {
        this.serviceName = serviceName;
    }

    public getVersion(): String {
        return this.version;
    }

    public setVersion(version: String): void {
        this.version = version;
    }

    public getGenericIdentifier(): number {
        return this.genericIdentifier;
    }

    public setGenericIdentifier(genericIdentifier: number): void {
        this.genericIdentifier = genericIdentifier;
    }

    public getServiceMethod(): String {
        return this.serviceMethod;
    }

    public setServiceMethod(serviceMethod: String): void {
        this.serviceMethod = serviceMethod;
    }


}