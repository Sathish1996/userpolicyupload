export class User_policy_document{
    
    public documentPath: string;

    public getDocumentPath(): string {
        return this.documentPath;
    }

    public setDocumentPath(documentPath: string): void {
        this.documentPath = documentPath;
    }


}