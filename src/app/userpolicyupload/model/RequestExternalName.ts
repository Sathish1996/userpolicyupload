export class RequestExternalName
{
    private clientId: String;

    private loggedInUserId: String;

    private serviceName: String;

    private genericIdentifiers:String[] ;

    private version: String;

    private serviceMethod: String;

    private clientKey: String;

    public getClientKey(): String {
        return this.clientKey;
    }

    public setClientKey(clientKey: String): void {
        this.clientKey = clientKey;
    }


    public getClientId(): String {
        return this.clientId;
    }

    public setClientId(clientId: String): void {
        this.clientId = clientId;
    }

    public getLoggedInUserId(): String {
        return this.loggedInUserId;
    }

    public setLoggedInUserId(loggedInUserId: String): void {
        this.loggedInUserId = loggedInUserId;
    }

    public getServiceName(): String {
        return this.serviceName;
    }

    public setServiceName(serviceName: String): void {
        this.serviceName = serviceName;
    }

    public getGenericIdentifiers(): String[] {
        return this.genericIdentifiers;
    }

    public setGenericIdentifiers(genericIdentifiers: String[]): void {
        this.genericIdentifiers = genericIdentifiers;
    }

    public getVersion(): String {
        return this.version;
    }

    public setVersion(version: String): void {
        this.version = version;
    }

    public getServiceMethod(): String {
        return this.serviceMethod;
    }

    public setServiceMethod(serviceMethod: String): void {
        this.serviceMethod = serviceMethod;
    }


}
