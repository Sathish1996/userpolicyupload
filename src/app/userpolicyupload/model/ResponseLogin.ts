export class ResponseLogin{
                     
    private lastName:String;

    private country:String;

    private kycStatus:String;

    private occupation:String;

    private annualIncome:String;

    private gender:String;

    private city:String;

    private roles:String;

    private emailId:String;

    private password:string;

    private isCorporate:String;

    private addressLine1:String;

    private aadhar:String;

    private addressLine2:String;

    private notifyMobile:String;

    private pan:String;

    private uniqueIdentifier:String;

    private clientId: string;

    private roleId:String;

    private mobileNo:String;

    private userName:String;

    private version:String;

    private userId:String;

    private firstName:String;

    private dob:String;

    private pinCode:String;

    private notifyEmail:String;

    private status:String;

    private errorMessage: String;

    private statusCode: String;

    private clientKey: String;

    public getClientKey(): String {
        return this.clientKey;
    }

    public setClientKey(clientKey: String): void {
        this.clientKey = clientKey;
    }



    public getErrorMessage(): String {
        return this.errorMessage;
    }

    public setErrorMessage(errorMessage: String): void {
        this.errorMessage = errorMessage;
    }

    public getStatusCode(): String {
        return this.statusCode;
    }

    public setStatusCode(statusCode: String): void {
        this.statusCode = statusCode;
    }


    public getLastName(): String {
        return this.lastName;
    }

    public setLastName(lastName: String): void {
        this.lastName = lastName;
    }

    public getCountry(): String {
        return this.country;
    }

    public setCountry(country: String): void {
        this.country = country;
    }

    public getKycStatus(): String {
        return this.kycStatus;
    }

    public setKycStatus(kycStatus: String): void {
        this.kycStatus = kycStatus;
    }

    public getOccupation(): String {
        return this.occupation;
    }

    public setOccupation(occupation: String): void {
        this.occupation = occupation;
    }

    public getAnnualIncome(): String {
        return this.annualIncome;
    }

    public setAnnualIncome(annualIncome: String): void {
        this.annualIncome = annualIncome;
    }

    public getGender(): String {
        return this.gender;
    }

    public setGender(gender: String): void {
        this.gender = gender;
    }

    public getCity(): String {
        return this.city;
    }

    public setCity(city: String): void {
        this.city = city;
    }

    public getRoles(): String {
        return this.roles;
    }

    public setRoles(roles: String): void {
        this.roles = roles;
    }

    public getEmailId(): String {
        return this.emailId;
    }

    public setEmailId(emailId: String): void {
        this.emailId = emailId;
    }

    public getPassword(): string {
        return this.password;
    }

    public setPassword(password: string): void {
        this.password = password;
    }

    public getIsCorporate(): String {
        return this.isCorporate;
    }

    public setIsCorporate(isCorporate: String): void {
        this.isCorporate = isCorporate;
    }

    public getAddressLine1(): String {
        return this.addressLine1;
    }

    public setAddressLine1(addressLine1: String): void {
        this.addressLine1 = addressLine1;
    }

    public getAadhar(): String {
        return this.aadhar;
    }

    public setAadhar(aadhar: String): void {
        this.aadhar = aadhar;
    }

    public getAddressLine2(): String {
        return this.addressLine2;
    }

    public setAddressLine2(addressLine2: String): void {
        this.addressLine2 = addressLine2;
    }

    public getNotifyMobile(): String {
        return this.notifyMobile;
    }

    public setNotifyMobile(notifyMobile: String): void {
        this.notifyMobile = notifyMobile;
    }

    public getPan(): String {
        return this.pan;
    }

    public setPan(pan: String): void {
        this.pan = pan;
    }

    public getUniqueIdentifier(): String {
        return this.uniqueIdentifier;
    }

    public setUniqueIdentifier(uniqueIdentifier: String): void {
        this.uniqueIdentifier = uniqueIdentifier;
    }

    public getClientId(): string {
        return this.clientId;
    }

    public setClientId(clientId: string): void {
        this.clientId = clientId;
    }

    public getRoleId(): String {
        return this.roleId;
    }

    public setRoleId(roleId: String): void {
        this.roleId = roleId;
    }

    public getMobileNo(): String {
        return this.mobileNo;
    }

    public setMobileNo(mobileNo: String): void {
        this.mobileNo = mobileNo;
    }

    public getUserName(): String {
        return this.userName;
    }

    public setUserName(userName: String): void {
        this.userName = userName;
    }

    public getVersion(): String {
        return this.version;
    }

    public setVersion(version: String): void {
        this.version = version;
    }

    public getUserId(): String {
        return this.userId;
    }

    public setUserId(userId: String): void {
        this.userId = userId;
    }

    public getFirstName(): String {
        return this.firstName;
    }

    public setFirstName(firstName: String): void {
        this.firstName = firstName;
    }

    public getDob(): String {
        return this.dob;
    }

    public setDob(dob: String): void {
        this.dob = dob;
    }

    public getPinCode(): String {
        return this.pinCode;
    }

    public setPinCode(pinCode: String): void {
        this.pinCode = pinCode;
    }

    public getNotifyEmail(): String {
        return this.notifyEmail;
    }

    public setNotifyEmail(notifyEmail: String): void {
        this.notifyEmail = notifyEmail;
    }

    public getStatus(): String {
        return this.status;
    }

    public setStatus(status: String): void {
        this.status = status;
    }

}