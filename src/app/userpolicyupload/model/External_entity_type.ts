import { InsuranceType } from './InsuranceType';

export class ExternalEntityType
{
    public externalEntityTypeId: number;
	public  entityType: String;
	public  icon: String;
	public  addedDate:Date;
	public  updatedDate:Date;
	public  createdBy:String;
	public  updatedBy:String;
	public insurance_type: InsuranceType;
	public  externalEntityTypeList:ExternalEntityType[];

    public getExternalEntityTypeId(): number {
        return this.externalEntityTypeId;
    }

    public setExternalEntityTypeId(externalEntityTypeId: number): void {
        this.externalEntityTypeId = externalEntityTypeId;
    }

    public getEntityType(): String {
        return this.entityType;
    }

    public setEntityType(entityType: String): void {
        this.entityType = entityType;
    }

    public getIcon(): String {
        return this.icon;
    }

    public setIcon(icon: String): void {
        this.icon = icon;
    }

    public getAddedDate(): Date {
        return this.addedDate;
    }

    public setAddedDate(addedDate: Date): void {
        this.addedDate = addedDate;
    }

    public getUpdatedDate(): Date {
        return this.updatedDate;
    }

    public setUpdatedDate(updatedDate: Date): void {
        this.updatedDate = updatedDate;
    }

    public getCreatedBy(): String {
        return this.createdBy;
    }

    public setCreatedBy(createdBy: String): void {
        this.createdBy = createdBy;
    }

    public getUpdatedBy(): String {
        return this.updatedBy;
    }

    public setUpdatedBy(updatedBy: String): void {
        this.updatedBy = updatedBy;
    }

    public getInsuranceTypeDTO(): InsuranceType {
        return this.insurance_type;
    }

    public setInsuranceTypeDTO(insurance_type: InsuranceType): void {
        this.insurance_type = insurance_type;
    }

    public getExternalEntityTypeDTOList(): ExternalEntityType[] {
        return this.externalEntityTypeList;
    }

    public setExternalEntityTypeDTOList(externalEntityTypeDTOList: ExternalEntityType[]): void {
        this.externalEntityTypeList = externalEntityTypeDTOList;
    }

	

   

}