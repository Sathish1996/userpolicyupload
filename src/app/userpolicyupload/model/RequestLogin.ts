export class RequestLogin{

private serviceName: String;
private serviceMethod:String;
private genericParam:String; 
private password:String;
private isPassword:boolean;
private roles:String;
private clientId:String;
private version:String;
private clientKey:string;

    public getClientKey(): string {
        return this.clientKey;
    }

    public setClientKey(clientKey: string): void {
        this.clientKey = clientKey;
    }


    public getServiceName(): String {
        return this.serviceName;
    }

    public setServiceName(serviceName: String): void {
        this.serviceName = serviceName;
    }

    public getServiceMethod(): String {
        return this.serviceMethod;
    }

    public setServiceMethod(serviceMethod: String): void {
        this.serviceMethod = serviceMethod;
    }

    public getGenericParam(): String {
        return this.genericParam;
    }

    public setGenericParam(genericParam: String): void {
        this.genericParam = genericParam;
    }

    public getPassword(): String {
        return this.password;
    }

    public setPassword(password: String): void {
        this.password = password;
    }

    public isIsPassword(): boolean {
        return this.isPassword;
    }

    public setIsPassword(isPassword: boolean): void {
        this.isPassword = isPassword;
    }

    public getRoles(): String {
        return this.roles;
    }

    public setRoles(roles: String): void {
        this.roles = roles;
    }

    public getClientId(): String {
        return this.clientId;
    }

    public setClientId(clientId: String): void {
        this.clientId = clientId;
    }

    public getVersion(): String {
        return this.version;
    }

    public setVersion(version: String): void {
        this.version = version;
    }

}