export class UserRole{
    private userId: number;
    private roleId: number;

    public getUserId(): number {
        return this.userId;
    }

    public setUserId(userId: number): void {
        this.userId = userId;
    }

    public getRoleId(): number {
        return this.roleId;
    }

    public setRoleId(roleId: number): void {
        this.roleId = roleId;
    }

}