export class Role{
    
    private roleId: number;
    private roleType: String;
    private addedDate: Date;
    private updatedDate: Date;
    private createdBy: String;
    private updatedBy: String;

    public getRoleId(): number {
        return this.roleId;
    }

    public setRoleId(roleId: number): void {
        this.roleId = roleId;
    }

    public getRoleType(): String {
        return this.roleType;
    }

    public setRoleType(roleType: String): void {
        this.roleType = roleType;
    }

    public getAddedDate(): Date {
        return this.addedDate;
    }

    public setAddedDate(addedDate: Date): void {
        this.addedDate = addedDate;
    }

    public getUpdatedDate(): Date {
        return this.updatedDate;
    }

    public setUpdatedDate(updatedDate: Date): void {
        this.updatedDate = updatedDate;
    }

    public getCreatedBy(): String {
        return this.createdBy;
    }

    public setCreatedBy(createdBy: String): void {
        this.createdBy = createdBy;
    }

    public getUpdatedBy(): String {
        return this.updatedBy;
    }

    public setUpdatedBy(updatedBy: String): void {
        this.updatedBy = updatedBy;
    }


}