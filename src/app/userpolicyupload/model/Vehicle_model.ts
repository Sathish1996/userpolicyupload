class Vehicle_model
{
    public vehicleModelId:String;

    public model: String;

    public getVehicleModelId(): String {
        return this.vehicleModelId;
    }

    public setVehicleModelId(vehicleModelId: String): void {
        this.vehicleModelId = vehicleModelId;
    }

    public getModel(): String {
        return this.model;
    }

    public setModel(model: String): void {
        this.model = model;
    }

}