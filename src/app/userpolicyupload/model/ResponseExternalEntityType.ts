import { ExternalEntityType } from './External_entity_type';

export class ResponseExternalEntityType{
    private clientId: String;

    private external_entity_type_list:ExternalEntityType[];

    private version: String;

    public getClientId(): String {
        return this.clientId;
    }

    public setClientId(clientId: String): void {
        this.clientId = clientId;
    }

    public getExternal_entity_type_list(): ExternalEntityType[] {
        return this.external_entity_type_list;
    }

    public setExternal_entity_type_list(external_entity_type: ExternalEntityType[]): void {
        this.external_entity_type_list = external_entity_type;
    }

    public getVersion(): String {
        return this.version;
    }

    public setVersion(version: String): void {
        this.version = version;
    }

}