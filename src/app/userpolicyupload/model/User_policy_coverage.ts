 export class UserPolicyCoverage
{
    private overseasMedicalExpenseCoverage: String;

    private deathSumAssured: String;

    private baggageDelayCoverage: String;

    private emergencyMedicalEvacuationCoverage: String;

    private travelDelay: String;

    private accidentalDeathSumAssured:String ;

    private lossOfBaggageAndBelongingCoverage:String ;

    private totalAndPermanentDisabilitySumAssured: String;

    private noClaimBonusInPercentage:String;

    private excessOrDiscountAmount:String;

    private insuredDeclared:String;

    private coverageType: String;

    private insuredOrCoverageAmount: String;

    private preExistingDisease: String;

    private maternityBenefits: String;

    private hospitalWardType: String;

    private alternativeAccommodationSumAssured: String;

    private householdContentSumAssured: String;

    private renovationSumAssured: String;

    private temporaryTotalDisablementBenefit: String;

    private temporaryTotalDisablementBenefitTerm: String;

    private accidentalHospitalDailyAllowance: String;

    private lumpSumMaturityBenefit: String;

    private maturityDate: String;

    private criticalIllnessSumAssured: String;

    private earlyCriticalIllnessSumAssured: String;

    private payOutAmount: String;

    private payOutFrequency: String;

    public getNoClaimBonusInPercentage(): String {
        return this.noClaimBonusInPercentage;
    }

    public setNoClaimBonusInPercentage(noClaimBonusInPercentage: String): void {
        this.noClaimBonusInPercentage = noClaimBonusInPercentage;
    }

    public getExcessOrDiscountAmount(): String {
        return this.excessOrDiscountAmount;
    }

    public setExcessOrDiscountAmount(excessOrDiscountAmount: String): void {
        this.excessOrDiscountAmount = excessOrDiscountAmount;
    }

    public getInsuredDeclared(): String {
        return this.insuredDeclared;
    }

    public setInsuredDeclared(insuredDeclared: String): void {
        this.insuredDeclared = insuredDeclared;
    }

    public getCoverageType(): String {
        return this.coverageType;
    }

    public setCoverageType(coverageType: String): void {
        this.coverageType = coverageType;
    }

    public getInsuredOrCoverageAmount(): String {
        return this.insuredOrCoverageAmount;
    }

    public setInsuredOrCoverageAmount(insuredOrCoverageAmount: String): void {
        this.insuredOrCoverageAmount = insuredOrCoverageAmount;
    }

    public getPreExistingDisease(): String {
        return this.preExistingDisease;
    }

    public setPreExistingDisease(preExistingDisease: String): void {
        this.preExistingDisease = preExistingDisease;
    }

    public getMaternityBenefits(): String {
        return this.maternityBenefits;
    }

    public setMaternityBenefits(maternityBenefits: String): void {
        this.maternityBenefits = maternityBenefits;
    }

    public getHospitalWardType(): String {
        return this.hospitalWardType;
    }

    public setHospitalWardType(hospitalWardType: String): void {
        this.hospitalWardType = hospitalWardType;
    }

    public getAlternativeAccommodationSumAssured(): String {
        return this.alternativeAccommodationSumAssured;
    }

    public setAlternativeAccommodationSumAssured(alternativeAccommodationSumAssured: String): void {
        this.alternativeAccommodationSumAssured = alternativeAccommodationSumAssured;
    }

    public getHouseholdContentSumAssured(): String {
        return this.householdContentSumAssured;
    }

    public setHouseholdContentSumAssured(householdContentSumAssured: String): void {
        this.householdContentSumAssured = householdContentSumAssured;
    }

    public getRenovationSumAssured(): String {
        return this.renovationSumAssured;
    }

    public setRenovationSumAssured(renovationSumAssured: String): void {
        this.renovationSumAssured = renovationSumAssured;
    }

    public getTemporaryTotalDisablementBenefit(): String {
        return this.temporaryTotalDisablementBenefit;
    }

    public setTemporaryTotalDisablementBenefit(temporaryTotalDisablementBenefit: String): void {
        this.temporaryTotalDisablementBenefit = temporaryTotalDisablementBenefit;
    }

    public getTemporaryTotalDisablementBenefitTerm(): String {
        return this.temporaryTotalDisablementBenefitTerm;
    }

    public setTemporaryTotalDisablementBenefitTerm(temporaryTotalDisablementBenefitTerm: String): void {
        this.temporaryTotalDisablementBenefitTerm = temporaryTotalDisablementBenefitTerm;
    }

    public getAccidentalHospitalDailyAllowance(): String {
        return this.accidentalHospitalDailyAllowance;
    }

    public setAccidentalHospitalDailyAllowance(accidentalHospitalDailyAllowance: String): void {
        this.accidentalHospitalDailyAllowance = accidentalHospitalDailyAllowance;
    }

    public getLumpSumMaturityBenefit(): String {
        return this.lumpSumMaturityBenefit;
    }

    public setLumpSumMaturityBenefit(lumpSumMaturityBenefit: String): void {
        this.lumpSumMaturityBenefit = lumpSumMaturityBenefit;
    }

    public getMaturityDate(): String {
        return this.maturityDate;
    }

    public setMaturityDate(maturityDate: String): void {
        this.maturityDate = maturityDate;
    }

    public getCriticalIllnessSumAssured(): String {
        return this.criticalIllnessSumAssured;
    }

    public setCriticalIllnessSumAssured(criticalIllnessSumAssured: String): void {
        this.criticalIllnessSumAssured = criticalIllnessSumAssured;
    }

    public getEarlyCriticalIllnessSumAssured(): String {
        return this.earlyCriticalIllnessSumAssured;
    }

    public setEarlyCriticalIllnessSumAssured(earlyCriticalIllnessSumAssured: String): void {
        this.earlyCriticalIllnessSumAssured = earlyCriticalIllnessSumAssured;
    }

    public getPayOutAmount(): String {
        return this.payOutAmount;
    }

    public setPayOutAmount(payOutAmount: String): void {
        this.payOutAmount = payOutAmount;
    }

    public getPayOutFrequency(): String {
        return this.payOutFrequency;
    }

    public setPayOutFrequency(payOutFrequency: String): void {
        this.payOutFrequency = payOutFrequency;
    }



    public getOverseasMedicalExpenseCoverage(): String {
        return this.overseasMedicalExpenseCoverage;
    }

    public setOverseasMedicalExpenseCoverage(overseasMedicalExpenseCoverage: String): void {
        this.overseasMedicalExpenseCoverage = overseasMedicalExpenseCoverage;
    }

    public getDeathSumAssured(): String {
        return this.deathSumAssured;
    }

    public setDeathSumAssured(deathSumAssured: String): void {
        this.deathSumAssured = deathSumAssured;
    }

    public getBaggageDelayCoverage(): String {
        return this.baggageDelayCoverage;
    }

    public setBaggageDelayCoverage(baggageDelayCoverage: String): void {
        this.baggageDelayCoverage = baggageDelayCoverage;
    }

    public getEmergencyMedicalEvacuationCoverage(): String {
        return this.emergencyMedicalEvacuationCoverage;
    }

    public setEmergencyMedicalEvacuationCoverage(emergencyMedicalEvacuationCoverage: String): void {
        this.emergencyMedicalEvacuationCoverage = emergencyMedicalEvacuationCoverage;
    }

    public getTravelDelay(): String {
        return this.travelDelay;
    }

    public setTravelDelay(travelDelay: String): void {
        this.travelDelay = travelDelay;
    }

    public getAccidentalDeathSumAssured(): String {
        return this.accidentalDeathSumAssured;
    }

    public setAccidentalDeathSumAssured(accidentalDeathSumAssured: String): void {
        this.accidentalDeathSumAssured = accidentalDeathSumAssured;
    }

    public getLossOfBaggageAndBelongingCoverage(): String {
        return this.lossOfBaggageAndBelongingCoverage;
    }

    public setLossOfBaggageAndBelongingCoverage(lossOfBaggageAndBelongingCoverage: String): void {
        this.lossOfBaggageAndBelongingCoverage = lossOfBaggageAndBelongingCoverage;
    }

    public getTotalAndPermanentDisabilitySumAssured(): String {
        return this.totalAndPermanentDisabilitySumAssured;
    }

    public setTotalAndPermanentDisabilitySumAssured(totalAndPermanentDisabilitySumAssured: String): void {
        this.totalAndPermanentDisabilitySumAssured = totalAndPermanentDisabilitySumAssured;
    }

}
