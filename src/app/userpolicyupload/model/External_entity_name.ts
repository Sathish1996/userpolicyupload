import { external_entity_name_list } from './External_Entity_Name_List';

export class External_entity_name
{
    private   external_entity_name_list:external_entity_name_list[];
    private   externalEntityNameId:String;

    public getExternal_entity_name_id(): String {
        return this.externalEntityNameId;
    }

    public setExternal_entity_name_id(externalEntityNameId: String): void {
        this.externalEntityNameId = externalEntityNameId;
    }


    public getExternal_entity_name_list(): external_entity_name_list[] {
        return this.external_entity_name_list;
    }

    public setExternal_entity_name_list(external_entity_name_list: external_entity_name_list[]): void {
        this.external_entity_name_list = external_entity_name_list;
    }

}