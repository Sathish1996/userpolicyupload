export class UserPolicyAgent
{private phone:String;

    private name:String;

    public getPhone(): String {
        return this.phone;
    }

    public setPhone(phone: String): void {
        this.phone = phone;
    }

    public getName(): String {
        return this.name;
    }

    public setName(name: String): void {
        this.name = name;
    }
}
