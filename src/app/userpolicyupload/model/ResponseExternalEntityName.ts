import { External_entity_name } from './External_entity_name';

export class ResponseExternalEntityName
{
    private external_entity_name:  External_entity_name;

    private clientId: String;

    private version: String;

    public getExternal_entity_name(): External_entity_name {
        return this.external_entity_name;
    }

    public setExternal_entity_name(external_entity_name: External_entity_name): void {
        this.external_entity_name = external_entity_name;
    }

    public getClientId(): String {
        return this.clientId;
    }

    public setClientId(clientId: String): void {
        this.clientId = clientId;
    }

    public getVersion(): String {
        return this.version;
    }

    public setVersion(version: String): void {
        this.version = version;
    }

}