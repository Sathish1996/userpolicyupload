import { UserDocumentList } from './UserDocumentList';

export class ResponseUserDocument
{
    private clientId: String;

    private userDocumentList: UserDocumentList[];

    private version: String;

    public getClientId(): String {
        return this.clientId;
    }

    public setClientId(clientId: String): void {
        this.clientId = clientId;
    }

    public getUserDocumentList(): UserDocumentList[] {
        return this.userDocumentList;
    }

    public setUserDocumentList(userDocumentList: UserDocumentList[]): void {
        this.userDocumentList = userDocumentList;
    }

    public getVersion(): String {
        return this.version;
    }

    public setVersion(version: String): void {
        this.version = version;
    }


}