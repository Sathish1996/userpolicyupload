export class ResponseVehicleModel
{
    private clientId: String;

    public vehicle_model:Vehicle_model[] ;

    private version:String;

    private clientKey: String;

    public getClientKey(): String {
        return this.clientKey;
    }

    public setClientKey(clientKey: String): void {
        this.clientKey = clientKey;
    }


    public getClientId(): String {
        return this.clientId;
    }

    public setClientId(clientId: String): void {
        this.clientId = clientId;
    }

    public getVehicle_model(): Vehicle_model[] {
        return this.vehicle_model;
    }

    public setVehicle_model(vehicle_model: Vehicle_model[]): void {
        this.vehicle_model = vehicle_model;
    }

    public getVersion(): String {
        return this.version;
    }

    public setVersion(version: String): void {
        this.version = version;
    }

}

class Vehicle_model
{
    public vehicleModelId:String;

    public model: String;

    public getVehicleModelId(): String {
        return this.vehicleModelId;
    }

    public setVehicleModelId(vehicleModelId: String): void {
        this.vehicleModelId = vehicleModelId;
    }

    public getModel(): String {
        return this.model;
    }

    public setModel(model: String): void {
        this.model = model;
    }

}
