

export class User{
   private userId: number;
   private landline: String;
   private mobileNo: String;
   private  password: String;
   private title: String;
   private  firstName: String;
   private lastName: String;
   private surName: String;
   private userName: String;
   private fatherName: String;
   private motherName: String;
   private userCode: String;
   private emailId: String;
   private dob: Date;
   private addressLine1: String;
   private addressLine2: String;
   private houseNumber: String;
   private country: String;
   private city: String;
   private district: String;
   private state: String;
   private pincode: String;
   private pan: String;
   private gender: String;
   private aadhar: String;
   private tan: String;
   private cin: String;
   private isMarried: String;
   private latestCibilScore: String;
   private anniversary: String;
   private status: String;
   private kycStatus: String;
   private occupation: String;
   private employment: String;
   private qualification: String;
   private nonProfit:String;
   private annualIncome: DoubleRange;
   private householdIncome: String;
   private notifyMobile: String;
   private notifyEmail: String;
   private isCorporate: String;
   private uniqueIdentifier: String;
   private fingerPrintImage: any;
   private image: any;
   private addedDate: Date;
   private updatedDate: Date;
   private createdBy: String;
   private updatedBy: String;


    public getUserId(): number {
        return this.userId;
    }

    public setUserId(userId: number): void {
        this.userId = userId;
    }

    public getLandline(): String {
        return this.landline;
    }

    public setLandline(landline: String): void {
        this.landline = landline;
    }

    public getMobileNo(): String {
        return this.mobileNo;
    }

    public setMobileNo(mobileNo: String): void {
        this.mobileNo = mobileNo;
    }

    public getPassword(): String {
        return this.password;
    }

    public setPassword(password: String): void {
        this.password = password;
    }

    public getTitle(): String {
        return this.title;
    }

    public setTitle(title: String): void {
        this.title = title;
    }

    public getFirstName(): String {
        return this.firstName;
    }

    public setFirstName(firstName: String): void {
        this.firstName = firstName;
    }

    public getLastName(): String {
        return this.lastName;
    }

    public setLastName(lastName: String): void {
        this.lastName = lastName;
    }

    public getSurName(): String {
        return this.surName;
    }

    public setSurName(surName: String): void {
        this.surName = surName;
    }

    public getUserName(): String {
        return this.userName;
    }

    public setUserName(userName: String): void {
        this.userName = userName;
    }

    public getFatherName(): String {
        return this.fatherName;
    }

    public setFatherName(fatherName: String): void {
        this.fatherName = fatherName;
    }

    public getMotherName(): String {
        return this.motherName;
    }

    public setMotherName(motherName: String): void {
        this.motherName = motherName;
    }

    public getUserCode(): String {
        return this.userCode;
    }

    public setUserCode(userCode: String): void {
        this.userCode = userCode;
    }

    public getEmailId(): String {
        return this.emailId;
    }

    public setEmailId(emailId: String): void {
        this.emailId = emailId;
    }

    public getDob(): Date {
        return this.dob;
    }

    public setDob(dob: Date): void {
        this.dob = dob;
    }

    public getAddressLine1(): String {
        return this.addressLine1;
    }

    public setAddressLine1(addressLine1: String): void {
        this.addressLine1 = addressLine1;
    }

    public getAddressLine2(): String {
        return this.addressLine2;
    }

    public setAddressLine2(addressLine2: String): void {
        this.addressLine2 = addressLine2;
    }

    public getHouseNumber(): String {
        return this.houseNumber;
    }

    public setHouseNumber(houseNumber: String): void {
        this.houseNumber = houseNumber;
    }

    public getCountry(): String {
        return this.country;
    }

    public setCountry(country: String): void {
        this.country = country;
    }

    public getCity(): String {
        return this.city;
    }

    public setCity(city: String): void {
        this.city = city;
    }

    public getDistrict(): String {
        return this.district;
    }

    public setDistrict(district: String): void {
        this.district = district;
    }

    public getState(): String {
        return this.state;
    }

    public setState(state: String): void {
        this.state = state;
    }

    public getPincode(): String {
        return this.pincode;
    }

    public setPincode(pincode: String): void {
        this.pincode = pincode;
    }

    public getPan(): String {
        return this.pan;
    }

    public setPan(pan: String): void {
        this.pan = pan;
    }

    public getGender(): String {
        return this.gender;
    }

    public setGender(gender: String): void {
        this.gender = gender;
    }

    public getAadhar(): String {
        return this.aadhar;
    }

    public setAadhar(aadhar: String): void {
        this.aadhar = aadhar;
    }

    public getTan(): String {
        return this.tan;
    }

    public setTan(tan: String): void {
        this.tan = tan;
    }

    public getCin(): String {
        return this.cin;
    }

    public setCin(cin: String): void {
        this.cin = cin;
    }

    public getIsMarried(): String {
        return this.isMarried;
    }

    public setIsMarried(isMarried: String): void {
        this.isMarried = isMarried;
    }

    public getLatestCibilScore(): String {
        return this.latestCibilScore;
    }

    public setLatestCibilScore(latestCibilScore: String): void {
        this.latestCibilScore = latestCibilScore;
    }

    public getAnniversary(): String {
        return this.anniversary;
    }

    public setAnniversary(anniversary: String): void {
        this.anniversary = anniversary;
    }

    public getStatus(): String {
        return this.status;
    }

    public setStatus(status: String): void {
        this.status = status;
    }

    public getKycStatus(): String {
        return this.kycStatus;
    }

    public setKycStatus(kycStatus: String): void {
        this.kycStatus = kycStatus;
    }

    public getOccupation(): String {
        return this.occupation;
    }

    public setOccupation(occupation: String): void {
        this.occupation = occupation;
    }

    public getEmployment(): String {
        return this.employment;
    }

    public setEmployment(employment: String): void {
        this.employment = employment;
    }

    public getQualification(): String {
        return this.qualification;
    }

    public setQualification(qualification: String): void {
        this.qualification = qualification;
    }

    public getNonProfit(): String {
        return this.nonProfit;
    }

    public setNonProfit(nonProfit: String): void {
        this.nonProfit = nonProfit;
    }

    public getAnnualIncome(): DoubleRange {
        return this.annualIncome;
    }

    public setAnnualIncome(annualIncome: DoubleRange): void {
        this.annualIncome = annualIncome;
    }

    public getHouseholdIncome(): String {
        return this.householdIncome;
    }

    public setHouseholdIncome(householdIncome: String): void {
        this.householdIncome = householdIncome;
    }

    public getNotifyMobile(): String {
        return this.notifyMobile;
    }

    public setNotifyMobile(notifyMobile: String): void {
        this.notifyMobile = notifyMobile;
    }

    public getNotifyEmail(): String {
        return this.notifyEmail;
    }

    public setNotifyEmail(notifyEmail: String): void {
        this.notifyEmail = notifyEmail;
    }

    public getIsCorporate(): String {
        return this.isCorporate;
    }

    public setIsCorporate(isCorporate: String): void {
        this.isCorporate = isCorporate;
    }

    public getUniqueIdentifier(): String {
        return this.uniqueIdentifier;
    }

    public setUniqueIdentifier(uniqueIdentifier: String): void {
        this.uniqueIdentifier = uniqueIdentifier;
    }

    public getFingerPrintImage(): any {
        return this.fingerPrintImage;
    }

    public setFingerPrintImage(fingerPrintImage: any): void {
        this.fingerPrintImage = fingerPrintImage;
    }

    public getImage(): any {
        return this.image;
    }

    public setImage(image: any): void {
        this.image = image;
    }

    public getAddedDate(): Date {
        return this.addedDate;
    }

    public setAddedDate(addedDate: Date): void {
        this.addedDate = addedDate;
    }

    public getUpdatedDate(): Date {
        return this.updatedDate;
    }

    public setUpdatedDate(updatedDate: Date): void {
        this.updatedDate = updatedDate;
    }

    public getCreatedBy(): String {
        return this.createdBy;
    }

    public setCreatedBy(createdBy: String): void {
        this.createdBy = createdBy;
    }

    public getUpdatedBy(): String {
        return this.updatedBy;
    }

    public setUpdatedBy(updatedBy: String): void {
        this.updatedBy = updatedBy;
    }




}