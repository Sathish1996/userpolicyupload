import { ExternalEntityType } from './External_entity_type';
import { UserPolicyCoverage } from './User_policy_coverage';
import { UserPolicyAgent } from './User_policy_agent';
import { User_relation } from './User_relation';
import { External_entity_name } from './External_entity_name';
import { User_policy_vehicle_details } from './User_policy_vehicle_details';
import { User } from './user';
import { User_policy_document } from './user_policy_document';

export class UserPolicy{
  
    private external_entity_name: External_entity_name;

    private clientId: String;

    private policyName: String;

    private user_relation:User_relation;

    private policyNumber: String;

    private policyEndDate: String;

    private policyStartDate: String;

    private loggedInUserId: String;

    private serviceName: String;

    private version: String;

    private policyPremium: String;

    private external_entity_type:ExternalEntityType;

    private user_policy_coverage:UserPolicyCoverage ;

    private user_policy_agent:UserPolicyAgent;

    private noOfPeopleCovered: String;

    private serviceMethod: String;

    private occupationClass: String;

    public user_policy_vehicle_details:User_policy_vehicle_details;

    public user:User;
    
    private userPolicyId: String;

    private genericIdentifier: String;
    
    public user_policy_document: User_policy_document[];

    public clientKey: String;

    public getClientKey(): String {
        return this.clientKey;
    }

    public setClientKey(clientKey: String): void {
        this.clientKey = clientKey;
    }





    public getUser_policy_document(): User_policy_document[] {
        return this.user_policy_document;
    }

    public setUser_policy_document(user_policy_document: User_policy_document[]): void {
        this.user_policy_document = user_policy_document;
    }

    public getGenericIdentifier(): String {
        return this.genericIdentifier;
    }

    public setGenericIdentifier(genericIdentifier: String): void {
        this.genericIdentifier = genericIdentifier;
    }


    public getUserPolicyId(): String {
        return this.userPolicyId;
    }

    public setUserPolicyId(userPolicyId: String): void {
        this.userPolicyId = userPolicyId;
    }


    public getUser(): User {
        return this.user;
    }

    public setUser(user: User): void {
        this.user = user;
    }


    public getUser_policy_vehicle_details(): User_policy_vehicle_details {
        return this.user_policy_vehicle_details;
    }

    public setUser_policy_vehicle_details(user_policy_vehicle_details: User_policy_vehicle_details): void {
        this.user_policy_vehicle_details = user_policy_vehicle_details;
    }


    public getOccupationClass(): String {
        return this.occupationClass;
    }

    public setOccupationClass(occupationClass: String): void {
        this.occupationClass = occupationClass;
    }


    public getExternal_entity_name(): External_entity_name {
        return this.external_entity_name;
    }

    public setExternal_entity_name(external_entity_name: External_entity_name): void {
        this.external_entity_name = external_entity_name;
    }

    public getClientId(): String {
        return this.clientId;
    }

    public setClientId(clientId: String): void {
        this.clientId = clientId;
    }

    public getPolicyName(): String {
        return this.policyName;
    }

    public setPolicyName(policyName: String): void {
        this.policyName = policyName;
    }

    public getUser_relation(): User_relation {
        return this.user_relation;
    }

    public setUser_relation(user_relation: User_relation): void {
        this.user_relation = user_relation;
    }

    public getPolicyNumber(): String {
        return this.policyNumber;
    }

    public setPolicyNumber(policyNumber: String): void {
        this.policyNumber = policyNumber;
    }

    public getPolicyEndDate(): String {
        return this.policyEndDate;
    }

    public setPolicyEndDate(policyEndDate: String): void {
        this.policyEndDate = policyEndDate;
    }

    public getPolicyStartDate(): String {
        return this.policyStartDate;
    }

    public setPolicyStartDate(policyStartDate: String): void {
        this.policyStartDate = policyStartDate;
    }

    public getLoggedInUserId(): String {
        return this.loggedInUserId;
    }

    public setLoggedInUserId(loggedInUserId: String): void {
        this.loggedInUserId = loggedInUserId;
    }

    public getServiceName(): String {
        return this.serviceName;
    }

    public setServiceName(serviceName: String): void {
        this.serviceName = serviceName;
    }

    public getVersion(): String {
        return this.version;
    }

    public setVersion(version: String): void {
        this.version = version;
    }

    public getPolicyPremium(): String {
        return this.policyPremium;
    }

    public setPolicyPremium(policyPremium: String): void {
        this.policyPremium = policyPremium;
    }

    public getExternal_entity_type(): ExternalEntityType {
        return this.external_entity_type;
    }

    public setExternal_entity_type(external_entity_type: ExternalEntityType): void {
        this.external_entity_type = external_entity_type;
    }

    public getUser_policy_coverage(): UserPolicyCoverage {
        return this.user_policy_coverage;
    }

    public setUser_policy_coverage(user_policy_coverage: UserPolicyCoverage): void {
        this.user_policy_coverage = user_policy_coverage;
    }

    public getUser_policy_agent(): UserPolicyAgent {
        return this.user_policy_agent;
    }

    public setUser_policy_agent(user_policy_agent: UserPolicyAgent): void {
        this.user_policy_agent = user_policy_agent;
    }

    public getNoOfPeopleCovered(): String {
        return this.noOfPeopleCovered;
    }

    public setNoOfPeopleCovered(noOfPeopleCovered: String): void {
        this.noOfPeopleCovered = noOfPeopleCovered;
    }

    public getServiceMethod(): String {
        return this.serviceMethod;
    }

    public setServiceMethod(serviceMethod: String): void {
        this.serviceMethod = serviceMethod;
    }


}