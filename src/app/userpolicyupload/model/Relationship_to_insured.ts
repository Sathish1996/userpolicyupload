export class Relationship_to_insured
{
    private relationshipToInsuredId:String;

    public relation: String;

    public getRelationshipToInsuredId(): String {
        return this.relationshipToInsuredId;
    }

    public setRelationshipToInsuredId(relationshipToInsuredId: String): void {
        this.relationshipToInsuredId = relationshipToInsuredId;
    }

    public getRelation(): String {
        return this.relation;
    }

    public setRelation(relation: String): void {
        this.relation = relation;
    }


}