export class User_relation
{
    private firstName:  String;

    private lastName: String;

    private dob: String;

    private nickName: String;

    private relation: String;

    public getFirstName(): String {
        return this.firstName;
    }

    public setFirstName(firstName: String): void {
        this.firstName = firstName;
    }

    public getLastName(): String {
        return this.lastName;
    }

    public setLastName(lastName: String): void {
        this.lastName = lastName;
    }

    public getDob(): String {
        return this.dob;
    }

    public setDob(dob: String): void {
        this.dob = dob;
    }

    public getNickName(): String {
        return this.nickName;
    }

    public setNickName(nickName: String): void {
        this.nickName = nickName;
    }

    public getRelation(): String {
        return this.relation;
    }

    public setRelation(relation: String): void {
        this.relation = relation;
    }

}