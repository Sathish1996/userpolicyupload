import { User } from './user';

export class UserDocumentList
{
    private userDocumentId:String;

    private user: User;

    private documentPath: String;

    public getUserDocumentId(): String {
        return this.userDocumentId;
    }

    public setUserDocumentId(userDocumentId: String): void {
        this.userDocumentId = userDocumentId;
    }

    public getUser(): User {
        return this.user;
    }

    public setUser(user: User): void {
        this.user = user;
    }

    public getDocumentPath(): String {
        return this.documentPath;
    }

    public setDocumentPath(documentPath: String): void {
        this.documentPath = documentPath;
    }


}