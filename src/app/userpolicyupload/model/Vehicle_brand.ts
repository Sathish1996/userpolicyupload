export class Vehicle_brand
{
    public vehicleBrandId: String;

    public brand: String;

    public getVehicleBrandId(): String {
        return this.vehicleBrandId;
    }

    public setVehicleBrandId(vehicleBrandId: String): void {
        this.vehicleBrandId = vehicleBrandId;
    }

    public getBrand(): String {
        return this.brand;
    }

    public setBrand(brand: String): void {
        this.brand = brand;
    }

}