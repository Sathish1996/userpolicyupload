import { Vehicle_brand } from './Vehicle_brand';

export class ResponseVehicleBrand
{
    public vehicle_brand: Vehicle_brand[];

    private clientId: String;

    private version: String;

    private clientKey: String;

    public getClientKey(): String {
        return this.clientKey;
    }

    public setClientKey(clientKey: String): void {
        this.clientKey = clientKey;
    }


    public getVehicle_brand(): Vehicle_brand[] {
        return this.vehicle_brand;
    }

    public setVehicle_brand(vehicle_brand: Vehicle_brand[]): void {
        this.vehicle_brand = vehicle_brand;
    }

    public getClientId(): String {
        return this.clientId;
    }

    public setClientId(clientId: String): void {
        this.clientId = clientId;
    }

    public getVersion(): String {
        return this.version;
    }

    public setVersion(version: String): void {
        this.version = version;
    }

}