export class InsuranceTypeRoot{
    private insuranceTypeRootId: String;
	private name: String;

    public getInsuranceTypeRootId(): String {
        return this.insuranceTypeRootId;
    }

    public setInsuranceTypeRootId(insuranceTypeRootId: String): void {
        this.insuranceTypeRootId = insuranceTypeRootId;
    }

    public getName(): String {
        return this.name;
    }

    public setName(name: String): void {
        this.name = name;
    }

}