import { InsuranceTypeAndRoot } from './InsuranceTypeAndRoot';

export class InsuranceType{
    private insuranceTypeId: number;
	private type:  String;
	private addedDate: Date;
	private updatedDate:Date;
	private createdBy: String;
	private updatedBy: String;

	public insurance_type_and_root: InsuranceTypeAndRoot[];

	private insuranceTypeList:  InsuranceType[];

    public getInsuranceTypeId(): number {
        return this.insuranceTypeId;
    }

    public setInsuranceTypeId(insuranceTypeId: number): void {
        this.insuranceTypeId = insuranceTypeId;
    }

    public getType(): String {
        return this.type;
    }

    public setType(type: String): void {
        this.type = type;
    }

    public getAddedDate(): Date {
        return this.addedDate;
    }

    public setAddedDate(addedDate: Date): void {
        this.addedDate = addedDate;
    }

    public getUpdatedDate(): Date {
        return this.updatedDate;
    }

    public setUpdatedDate(updatedDate: Date): void {
        this.updatedDate = updatedDate;
    }

    public getCreatedBy(): String {
        return this.createdBy;
    }

    public setCreatedBy(createdBy: String): void {
        this.createdBy = createdBy;
    }

    public getUpdatedBy(): String {
        return this.updatedBy;
    }

    public setUpdatedBy(updatedBy: String): void {
        this.updatedBy = updatedBy;
    }

    public getInsuranceTypeAndRootList(): InsuranceTypeAndRoot[] {
        return this.insurance_type_and_root;
    }

    public setInsuranceTypeAndRootList(insurance_type_and_root: InsuranceTypeAndRoot[]): void {
        this.insurance_type_and_root = insurance_type_and_root;
    }

    public getInsuranceTypeList(): InsuranceType[] {
        return this.insuranceTypeList;
    }

    public setInsuranceTypeList(insuranceTypeList: InsuranceType[]): void {
        this.insuranceTypeList = insuranceTypeList;
    }

}