import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestLogin } from './model/RequestLogin';
import { ResponseLogin } from './model/ResponseLogin';
import { Router } from '@angular/router';
import { AuthGuardService } from './auth-guard.service';
import { Observable } from 'rxjs';
import { BaseServiceService } from './base-service.service';


@Injectable({
  providedIn: 'root'
})
export class ValidateLoginService {


  //baseURL: string = "https://cors-anywhere.herokuapp.com/https://www.finsall.co.in/FinsAllServer/api";

  //baseURL: string = 'https://www.finsall.co.in/FinsAllServer/api';

 private options = { headers: new HttpHeaders().set('Content-Type', 'application/json')
 .set('authentication-token','Ti1mR4QCFuy/Qj9ciqMj8OS6fg31oWL09THYFJHl5KM=').set('authentication-username','finsall')
  };

  
  validateWithMobileNo(genericParam: String, password: String) :Observable<ResponseLogin>
 {
  this.login.setServiceName("UserService");
  this.login.setGenericParam(genericParam);
  this.login.setIsPassword(true);
  this.login.setPassword(password);
  this.login.setServiceMethod("login");
  this.login.setRoles("Data Entry");
  this.login.setClientId("2019");
  this.login.setVersion("1.0.0");
  this.login.setClientKey("f6b720f6707cd1c29b5bd3b5123f8115aeef13d8f34f9a757c0dca91c3e26af3");
  console.log(JSON.stringify(this.login))
  return this.http.post<ResponseLogin>(this.baseService.baseUrl(),JSON.stringify(this.login),this.options);
  
 }
  constructor(private http: HttpClient,private login:RequestLogin,  private response: ResponseLogin,
    private router: Router, private authGuard: AuthGuardService,private baseService:BaseServiceService ) { }
}
