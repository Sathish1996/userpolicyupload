import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { from } from 'rxjs';
import { LoginMobilenoComponent } from './login-mobileno/login-mobileno.component'
import { UserDetailsTableComponent } from './user-details-table/user-details-table.component'
import { UserFormComponent } from './user-form/user-form.component';

const routes: Routes = [
  {path: '',pathMatch:'full',redirectTo:'login'},
  {path: 'login',component: LoginMobilenoComponent},
  {path: 'userdetailstable',component: UserDetailsTableComponent},
  {path: 'userform',component: UserFormComponent}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserpolicyuploadRoutingModule { }
