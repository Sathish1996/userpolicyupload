import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuardService } from './userpolicyupload/auth-guard.service';
import { BaseServiceService } from './userpolicyupload/base-service.service';
import { RequestLogin } from './userpolicyupload/model/RequestLogin';
import { ResponseLogin } from './userpolicyupload/model/ResponseLogin';
import { ValidateLoginService } from './userpolicyupload/validate-login.service';
import { RequestUserDocumentService } from './userpolicyupload/request-user-document.service';
import { ResponseUserDocument } from './userpolicyupload/model/ResponseUserDocument';
import { UserPolicy } from './userpolicyupload/model/UserPolicy';
import { User } from './userpolicyupload/model/user';
import { ExternalEntityType } from './userpolicyupload/model/External_entity_type';
import { External_entity_name } from './userpolicyupload/model/External_entity_name';
import { Relation } from './userpolicyupload/model/Relation';
import { User_policy_document } from './userpolicyupload/model/user_policy_document';
import { RequestDocument } from './userpolicyupload/model/RequestDocument';
import { LoginMobilenoComponent } from './userpolicyupload/login-mobileno/login-mobileno.component';
import { SubmitService } from './userpolicyupload/submit.service';
import { RequestRelation } from './userpolicyupload/model/RequestRelation';
import { VehicleDetailsService } from './userpolicyupload/vehicle-details.service';
import { InsuranceName } from './userpolicyupload/insuranceName';
import { RequestVehicleModel } from './userpolicyupload/model/RequestVehicleModel';
import { RequestVehicleBrand } from './userpolicyupload/model/RequestVehicleBrand';
import { ResponseExternalEntityName } from './userpolicyupload/model/ResponseExternalEntityName';
import { User_relation } from './userpolicyupload/model/User_relation';
import { ResponseVehicleBrand } from './userpolicyupload/model/ResponseVehicleBrand';
import { external_entity_name_list } from './userpolicyupload/model/External_Entity_Name_List';
import { UserPolicyCoverage } from './userpolicyupload/model/User_policy_coverage';
import { UserPolicyAgent } from './userpolicyupload/model/User_policy_agent';
import { User_policy_vehicle_details } from './userpolicyupload/model/User_policy_vehicle_details';
import { ResponseVehicleModel } from './userpolicyupload/model/ResponseVehicleModel';
import { NgxImageZoomModule } from 'ngx-image-zoom';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { DataService } from './userpolicyupload/dataServicefortoggle';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgxImageZoomModule,
    NgbModule
  ],
  providers: [
    LoginMobilenoComponent,
    ValidateLoginService,
    AuthGuardService,
    BaseServiceService,
    ResponseLogin,
    RequestLogin,
    RequestUserDocumentService,
    ResponseUserDocument,
    UserPolicy,
    User,
    ExternalEntityType,
    External_entity_name,
    Relation,
    User_policy_document,
    RequestDocument,
    SubmitService,
    RequestRelation,
    VehicleDetailsService,
    InsuranceName,
    RequestVehicleModel,
    RequestVehicleBrand,
    ResponseExternalEntityName,
    User_relation,
    ResponseVehicleBrand,
    external_entity_name_list,
    UserPolicyCoverage,
    UserPolicyAgent,
    User_policy_vehicle_details,
    ResponseVehicleModel,
    DataService

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
