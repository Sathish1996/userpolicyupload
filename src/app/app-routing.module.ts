import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [{ path: 'userpolicyupload', loadChildren: () => import('./userpolicyupload/userpolicyupload.module').then(m => m.UserpolicyuploadModule) }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
